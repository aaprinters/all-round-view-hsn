<?php
/**
 * News Ticker widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_news_ticker_widgets() {
	register_widget( 'reendex_News_Ticker_Widget' );
}
add_action( 'widgets_init', 'reendex_news_ticker_widgets' );

	/**
	 * News Ticker widget class.
	 *
	 * @since  1.0
	 */
class Reendex_News_Ticker_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'reendex-news-ticker-widget',
			'description'   => esc_html__( 'All Round View: News Ticker Widget', 'reendex' ),
		);
		$control_ops = array(
			'id_base' => 'reendex-news-ticker-widget',
			);
		parent::__construct( 'reendex-news-ticker-widget', esc_html( 'All Round View: News Ticker' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for News Ticker widget instance.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		echo "\n" . '
		<div class="news-ticker-rss-widget widget container-wrapper">' . "\n";
		if ( $title ) {
				echo wp_kses_post( $args['before_title'] ) . esc_html( $title ) . wp_kses_post( $args['after_title'] );
		}
		ticker_rss_parser( $instance );
		echo "\n" . '</div>
		' . "\n";
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['urls'] = stripslashes( $new_instance['urls'] );
		$instance['count'] = absint( $new_instance['count'] );
		$instance['show_date'] = sanitize_text_field( $new_instance['show_date'] );
		$instance['show_desc'] = sanitize_text_field( $new_instance['show_desc'] );
		$instance['open_newtab'] = intval( $new_instance['open_newtab'] );
		$instance['strip_desc'] = intval( $new_instance['strip_desc'] );
		$instance['strip_title'] = intval( $new_instance['strip_title'] );
		$instance['read_more'] = stripslashes( $new_instance['read_more'] );
		$instance['color_style'] = stripslashes( $new_instance['color_style'] );
		$instance['enable_ticker'] = intval( $new_instance['enable_ticker'] );
		$instance['visible_items'] = sanitize_text_field( $new_instance['visible_items'] );
		$instance['ticker_speed'] = intval( $new_instance['ticker_speed'] );

		return $instance;
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options.
	 */
	public function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, ticker_defaults() );

		$title = htmlspecialchars( $instance['title'] );
		$urls = htmlspecialchars( $instance['urls'] );

		$count = intval( $instance['count'] );
		$show_date = intval( $instance['show_date'] );
		$show_desc = intval( $instance['show_desc'] );
		$open_newtab = intval( $instance['open_newtab'] );
		$strip_desc = intval( $instance['strip_desc'] );
		$strip_title = intval( $instance['strip_title'] );
		$read_more = htmlspecialchars( $instance['read_more'] );

		$color_style = stripslashes( $instance['color_style'] );
		$enable_ticker = intval( $instance['enable_ticker'] );
		$visible_items = intval( $instance['visible_items'] );
		$ticker_speed = intval( $instance['ticker_speed'] );

		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" /> 
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'urls' ) ); ?>">
				<?php esc_html_e( 'Feed URL:','reendex' ); ?>
			</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'urls' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'urls' ) ); ?>" value="<?php echo esc_attr( $instance['urls'] ); ?>" />
			<br/>
			<small>Example: http://feeds.reuters.com/reuters/businessNews</small></br>
		</p>
	<div class="ticker_settings">
		<h4>Settings</h4>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_desc' ) ); ?>" type="checkbox"  name="<?php echo esc_attr( $this->get_field_name( 'show_desc' ) ); ?>" value="1" <?php checked( $show_desc ) ?> />
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_desc' ) ); ?>">Show Description</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>">Total number of items to display:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="number" value="<?php echo esc_attr( $count ); ?>" class="widefat" title="The number of feed items to parse"/>
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" type="checkbox"  name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" value="1" <?php checked( $show_date ) ?> />
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>">Show Date</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'strip_desc' ) ); ?>">Strip content characters:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'strip_desc' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'strip_desc' ) ); ?>" type="number" value="<?php echo esc_attr( $strip_desc ); ?>" class="widefat" title="The number of charaters to be displayed. Use 0 to disable stripping"/>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'strip_title' ) ); ?>">Strip title characters:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'strip_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'strip_title' ) ); ?>" type="number" value="<?php echo esc_attr( $strip_title ); ?>" class="widefat" title="The number of charaters to be displayed. Use 0 to disable stripping"/>
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'open_newtab' ) ); ?>" type="checkbox"  name="<?php echo esc_attr( $this->get_field_name( 'open_newtab' ) ); ?>" value="1" <?php checked( $open_newtab ) ?> />
			<label for="<?php echo esc_attr( $this->get_field_id( 'open_newtab' ) ); ?>">Open links in new tab</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'read_more' ) ); ?>">Read more text:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'read_more' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'read_more' ) ); ?>" type="text" value="<?php echo esc_attr( $read_more ); ?>" class="widefat" title="Leave blank to hide read more text"/>
		</p>
	</div>
	<div class="ticker_settings">
		<h4>Other settings</h4>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'color_style' ) ); ?>">Feed style: </label>
			<?php
				$color_styles = ticker_color_styles();
				echo '<select name="' . esc_attr( $this->get_field_name( 'color_style' ) ) . '" id="' . esc_attr( $this->get_field_id( 'color_style' ) ) . '">';
			foreach ( $color_styles as $k => $v ) {
					echo '<option value="' . esc_attr( $v ) . '" ' . ( $color_style == $v ? 'selected="selected"' : '' ) . '>' . esc_attr( $k ) . '</option>';
			}
				echo '</select>';
			?>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'visible_items' ) ); ?>">Visible items:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'visible_items' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'visible_items' ) ); ?>" type="number" value="<?php echo esc_attr( $visible_items ); ?>" title="The number of feed items to be visible."/>
			<br/>
			<small>The number of feed items to be visible</small></br>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'enable_ticker' ) ); ?>">Ticker animation:</label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'enable_ticker' ) ); ?>" type="checkbox"  name="<?php echo esc_attr( $this->get_field_name( 'enable_ticker' ) ); ?>" value="1" <?php checked( $enable_ticker ) ?> />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'ticker_speed' ) ); ?>">Ticker speed: </label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'ticker_speed' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'ticker_speed' ) ); ?>" type="number" value="<?php echo esc_attr( $ticker_speed ); ?>" title="Speed of the ticker in seconds"/>
			seconds </p>
	</div>
	<?php
	}
}
?>
