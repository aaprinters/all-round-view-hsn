<?php
/**
 * All Round View widgets inclusion
 *
 * This is the template that includes all custom widgets of All Round View
 *
 * @package All Round View
 * @since All Round View 1.0
 */

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function reendex_widgets_init() {

	// Register Sidebar widget.
	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar Widget Area', 'reendex' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'reendex' ),
		'before_widget'     => '<div id="%1$s" class="widget %2$s">',
		'after_widget'      => '</div>',
		'before_title'      => '<h4 class="widget-title">',
		'after_title'       => '</h4>',
	) );

	// Register Footer widgets.
	register_sidebar( array(
		'name'            => esc_attr__( 'Footer Column One', 'reendex' ),
		'id'              => 'footer_1',
		'before_widget'   => '<aside id="%1$s" class="%2$s container-wrapper">',
		'after_widget'    => '</aside>',
		'before_title'    => '<h4 class="widget-title">',
		'after_title'     => '</h4>',
	));
	register_sidebar( array(
		'name'             => esc_attr__( 'Footer Column Two', 'reendex' ),
		'id'               => 'footer_2',
		'before_widget'    => '<aside id="%1$s" class="%2$s container-wrapper">',
		'after_widget'     => '</aside>',
		'before_title'     => '<h3 class="widget-title">',
		'after_title'      => '</h3>',
	));
	register_sidebar(array(
		'name'             => esc_attr__( 'Footer Column Three', 'reendex' ),
		'id'               => 'footer_3',
		'before_widget'    => '<aside id="%1$s" class="%2$s container-wrapper">',
		'after_widget'     => '</aside>',
		'before_title'     => '<h3 class="widget-title">',
		'after_title'      => '</h3>',
	));
	register_sidebar(array(
		'name'             => esc_attr__( 'Footer Column Four', 'reendex' ),
		'id'               => 'footer_4',
		'before_widget'    => '<aside id="%1$s" class="%2$s container-wrapper">',
		'after_widget'     => '</aside>',
		'before_title'     => '<h3 class="widget-title">',
		'after_title'      => '</h3>',
	));
	register_sidebar(array(
		'name'             => esc_attr__( 'Footer Column Five', 'reendex' ),
		'id'               => 'footer_5',
		'before_widget'    => '<aside id="%1$s" class="%2$s container-wrapper">',
		'after_widget'     => '</aside>',
		'before_title'     => '<h3 class="widget-title">',
		'after_title'      => '</h3>',
	));

	// Register MegaMenu widget.
	$location = 'mainmenu';
	$css_class = 'mega-menu-parent';
	$locations = get_nav_menu_locations();
	if ( isset( $locations[ $location ] ) ) {
		$menu = get_term( $locations[ $location ], 'nav_menu' );
		$items = wp_get_nav_menu_items( $menu->name );
		if ( $items ) {
			foreach ( $items as $item ) {
				if ( in_array( $css_class, $item->classes ) ) {
					register_sidebar( array(
						'name'            => $item->title . ' - Mega Menu',
						'id'              => 'mega-menu-item-' . $item->ID,
						'description'     => 'Mega Menu items',
						'before_widget'   => '<li id="%1$s" class="mega-menu-item">',
						'after_widget'    => '</li>',
					));
				}
			}
		}
	}
}
add_action( 'widgets_init', 'reendex_widgets_init' );
