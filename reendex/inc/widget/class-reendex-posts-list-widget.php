<?php
/**
 * Posts List widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_posts_list_widgets() {
	register_widget( 'Reendex_Posts_List_Widget' );
}
	add_action( 'widgets_init', 'reendex_posts_list_widgets' );

if ( ! class_exists( 'Reendex_Posts_List_Widget' ) ) {

	/**
	 * Core class used to implement the Mega menu elements widget.
	 *
	 * @since  1.0
	 */
	class Reendex_Posts_List_Widget extends WP_Widget {

		/**
		 * Constructor.
		 */
		function __construct() {
			$widget_ops = array(
				'classname'     => 'reendex-posts-list-widget',
				'description'   => esc_html__( 'All Round View: Posts List With Thumbnails Widget','reendex'
				),
			);
			$control_ops = array(
				'id_base' => 'reendex-posts-list-widget',
				);
			parent::__construct( 'reendex-posts-list-widget', esc_html( 'All Round View: Posts List With Thumbnails' ), $widget_ops, $control_ops );
		}

		/**
		 * Outputs the content for the current Posts List widget instance.
		 *
		 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
		 * @param array $instance Settings for Posts List widget instance.
		 */
		function widget( $args, $instance ) {
			$title          = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
			$limit          = ( 0 !== $instance['limit'] )? absint( $instance['limit'] ) : 4;
			$order          = $instance['order'];
			$orderby        = $instance['orderby'];
			$categories     = isset( $instance['categories'] ) ? (array) $instance['categories'] : array();
			$show_category  = empty( $instance['show_category'] ) ? 0 : $instance['show_category'];
			$excerpt_words  = absint( $instance['excerpt_words'] );
			$extclass       = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;

			if ( isset( $before_widget ) ) {
				echo wp_kses( $before_widget, 'aside' );
			}

				$args = array(
					'post_type'				=> 'post',
					'ignore_sticky_posts'	=> 1,
					'post_status'			=> 'publish',
					'posts_per_page'		=> $limit,
					'order'				    => $order,
					'orderby'				=> $orderby,
					'tax_query' => array(
						array(
							'taxonomy'  => 'post_format',
							'field'     => 'slug',
							'terms'     => 'post-format-video',
							'operator'  => 'NOT IN',
						),
					),
				);

			if ( is_array( $categories ) && count( $categories ) > 0 ) {
				$args['category__in'] = $categories;
			}

			global $post;
			$posts = new WP_Query( $args );
			if ( $posts->have_posts() ) :
				$count = 0;
				$num_posts = $posts->post_count;
				?>
				<li class="reendex-widget <?php echo esc_attr( $extclass ); ?> widget container-wrapper">
					<div>
						<h4 class="widget-title"><?php echo esc_attr( $title );?></h4>
							<ul class="media-list">
								<?php $bg = 0; while ( $posts->have_posts() ) :
									$posts->the_post();
									$post_id = get_the_ID();
									$post_date = get_the_date( 'd-m-Y' );
									$author = get_field( 'hide_author',$post_id);
									$post_obj = get_post($post_id); // gets author from post
									$authid = $post_obj->post_author;
									$author_check = $author[0];
									$author_name = get_the_author_meta('display_name',$authid);
									$author_url = get_author_posts_url($authid );					
								?>
									<li class="media"> 
										<a class="pull-left" href="<?php the_permalink();?>">
											<?php
												the_post_thumbnail( 'reendex_recent_post_thumb', array(
													'class' => 'img-responsive img-full img-not-lazy inner_page_left_sidebar',
													)
												);
											?>
										</a>
										<div class="media-body">
											<p>
												<a target="_blank" href="<?php echo esc_url( get_permalink() ); ?>">
												<span class="bg-1 post_title_sidebar widget"><?php echo get_the_title(); ?></span>
												</a>
												
											</p>
											<p>
												<?php
												/*if ( 1 == $show_category ) {
													$count = 0;
													foreach ( ( get_the_category() ) as $category ) {
														$count ++ ;
														$category_id = get_cat_ID( $category->cat_name );
														$category_link = get_category_link( $category_id );
														if ( 1 === $count ) {
												?>
												<a target="_blank" href="<?php echo esc_url( $category_link ); ?>">
													<span class="bg-<?php echo esc_attr( $bg + 1 );?>"><?php echo esc_html( $category->cat_name );?></span>
												</a>
														<?php
														}
													}
												}*/
												?>
												<a class="content" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $excerpt_words ) ); ?></a>
												<div class="sidebar_date_views">
												<?
												
													if( $author_check == 'yes' ) {
															
													}else{
														?>
														<div class="author">
														<i class="fa fa-user"></i>
														<a class="author_link" href="<?php echo $author_url; ?>">
														<?php echo $author_name; ?>
														</a>
														</div>
														<?php
													}
														?>
																		
													<div class="date"><i class="fa fa-clock-o"></i>  <?php echo $post_date; ?></div>
													<div class="views"><span class="label_views"><i class="fa fa-eye"></i><?php echo do_shortcode('[views id="'.$post_id.'"]'); ?>Views</span></div>
														
												</div>
											</p>
										</div>
									</li><!-- /.media -->

								<?php $bg++;
								endwhile; ?>
							</ul><!-- /.media-list -->
					</div>
				</li><!-- /.reendex-widget -->
				<?php
			endif;

			if ( isset( $after_widget ) ) {
				echo wp_kses( $after_widget, 'aside' );
			}
			wp_reset_postdata();
		}
		/**
		 * Handles updating the settings for the current Posts List widget instance.
		 *
		 * @param array $new_instance New settings for this instance as input by the user via
		 *                            WP_Widget::form().
		 * @param array $old_instance Old settings for this instance.
		 * @return array Updated settings to save.
		 */
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] 			= sanitize_text_field( $new_instance['title'] );
			$instance['limit'] 			= absint( $new_instance['limit'] );
			$instance['order'] 			= sanitize_text_field( $new_instance['order'] );
			$instance['orderby'] 		= sanitize_text_field( $new_instance['orderby'] );
			$instance['categories'] 	= reendex_sanitize_multiple_checkbox( $new_instance['categories'] );
			$instance['show_category'] 	= (bool) $new_instance['show_category'] ? 1 : 0;
			$instance['excerpt_words']  = absint( $new_instance['excerpt_words'] );
			$instance['extclass']       = sanitize_text_field( $new_instance['extclass'] );
			return $instance;
		}
		/**
		 * Outputs the settings form for the Mega menu elements widget.
		 *
		 * @param array $instance Current settings.
		 */
		function form( $instance ) {

			$defaults = array(
				'title' 		=> esc_html__( 'Latest News', 'reendex' ),
				'limit'			=> 4,
				'order'			=> 'desc',
				'orderby'		=> 'date',
				'categories'	=> array(),
				'show_category' => 1,
				'excerpt_words' => 50,
				'extclass' 	    => '',
			);

			$instance = wp_parse_args( (array) $instance, $defaults );
			$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
			$categories = $this->reendex_get_list_categories( 0 );
			if ( ! is_array( $instance['categories'] ) ) {
				$instance['categories'] = array();
			}

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Enter your title', 'reendex' ); ?> </label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php esc_html_e( 'Number of posts to show', 'reendex' ); ?> </label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['limit'] ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_html_e( 'Order by', 'reendex' ); ?> </label>
				<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'orderby' ) ); ?>">
					<option value="none" <?php selected( 'none', $instance['orderby'] ) ?>><?php esc_html_e( 'None','reendex' );?></option>
					<option value="ID" <?php selected( 'ID', $instance['orderby'] ) ?>><?php esc_html_e( 'ID','reendex' );?></option>
					<option value="title" <?php selected( 'title', $instance['orderby'] ) ?>><?php esc_html_e( 'Title','reendex' );?></option>
					<option value="date" <?php selected( 'date', $instance['orderby'] ) ?>><?php esc_html_e( 'Date','reendex' );?></option>
					<option value="comment_count" <?php selected( 'comment_count', $instance['orderby'] ) ?>><?php esc_html_e( 'Comment count','reendex' );?></option>
					<option value="rand" <?php selected( 'rand', $instance['orderby'] ) ?>><?php esc_html_e( 'Random','reendex' );?></option>
				</select>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php esc_html_e( 'Order', 'reendex' ); ?> </label>
				<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>">
					<option value="asc" <?php selected( 'asc', $instance['order'] ) ?>>Ascending</option>
					<option value="desc" <?php selected( 'desc', $instance['order'] ) ?>>Descending</option>
				</select>
			</p>
			<p>
				<label><?php esc_html_e( 'Select categories', 'reendex' ); ?></label>
				<div class="categorydiv">
					<div class="tabs-panel">
						<ul class="categorychecklist">
							<?php foreach ( $categories as $cat ) { ?>
							<li>
								<label>
									<input type="checkbox"<?php checked( in_array( $cat->term_id, $instance['categories'] ) ); ?> name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $cat->term_id ); ?>]" value="<?php echo esc_attr( $cat->term_id ); ?>" />
									<?php echo esc_html( $cat->name ); ?>
								</label>
								<?php $this->reendex_get_list_sub_categories( $cat->term_id, $instance ); ?>
							</li>
							<?php } ?>
						</ul><!-- /.categorychecklist -->
					</div><!-- /.tabs-panel -->
				</div><!-- /.categorydiv -->
			</p>
			<p>
				<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'show_category' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_category' ) ); ?>" value="1" <?php checked( $instance['show_category'] ); ?> />
				<label for="<?php echo esc_attr( $this->get_field_id( 'show_category' ) ); ?>"><?php esc_html_e( 'Show Category', 'reendex' ); ?></label>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'excerpt_words' ) ); ?>"><?php esc_html_e( 'Content excerpt length', 'reendex' ); ?> </label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'excerpt_words' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'excerpt_words' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['excerpt_words'] ); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
				<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
			</p>			
			<?php
		}
		/**
		 * Returns a list of categories.
		 *
		 * @param string $cat_parent_id The parent term ID.
		 */
		function reendex_get_list_categories( $cat_parent_id ) {
			$args = array(
					'hierarchical'  => 1,
					'parent'		=> $cat_parent_id,
					'title_li'		=> '',
					'child_of'		=> 0,
				);
			$cats = get_categories( $args );
			return $cats;
		}

		/**
		 * Returns a list of subcategories.
		 *
		 * @param string $cat_parent_id The parent term ID.
		 * @param array  $instance Settings for the current Categories widget instance.
		 */
		function reendex_get_list_sub_categories( $cat_parent_id, $instance ) {
			$sub_categories = $this->reendex_get_list_categories( $cat_parent_id );
			if ( count( $sub_categories ) > 0 ) {
			?>
				<ul class="children">
					<?php foreach ( $sub_categories as $sub_cat ) { ?>
						<li>
							<label>
								<input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $sub_cat->term_id ); ?>]" value="<?php echo esc_attr( $sub_cat->term_id ); ?>" <?php checked( in_array( $sub_cat->term_id, $instance['categories'] ) ); ?> />
								<?php echo esc_html( $sub_cat->name ); ?>
							</label>
							<?php $this->reendex_get_list_sub_categories( $sub_cat->term_id, $instance ); ?>
						</li>
					<?php } ?>
				</ul><!-- /.children -->
			<?php }
		}

	}
} // End if().
