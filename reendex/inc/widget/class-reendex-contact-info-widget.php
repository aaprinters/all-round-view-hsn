<?php
/**
 * Contact Info widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function reendex_contact_info_widgets() {
	register_widget( 'reendex_Contact_Info_Widget' );
}
	add_action( 'widgets_init', 'reendex_contact_info_widgets' );

	/**
	 * Contact Info widget class.
	 *
	 * @since  1.0
	 */
class Reendex_Contact_Info_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'contact_info',
			'description'   => esc_html__( 'All Round View: Contact Info Widget','reendex' ),
			);
		$control_ops = array(
			'id_base' => 'contact_info-widget',
			);
		parent::__construct( 'contact_info-widget', esc_html__( 'All Round View: Contact Info','reendex' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for Contact Info widget instance.
	 */
	function widget( $args, $instance ) {
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : 0;
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'reendex_contact_info', 'widget' );
		}
		if ( ! is_array( $cache ) ) {
			$cache = array();
		}
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}
		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo wp_kses( $cache[ $args['widget_id'] ], wp_kses_allowed_html( 'post' ) );
			return;
		}
		ob_start();

		$title 		= ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title 		= apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$address   	= isset( $instance['address'] ) ? esc_html( $instance['address'] ) : '';
		$phone   	= isset( $instance['phone'] ) ? esc_html( $instance['phone'] ) : '';
		$email   	= isset( $instance['email'] ) ? antispambot( esc_html( $instance['email'] ) ) : '';

		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>
		<div class="contact_info <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
			<?php
			if ( $title ) {
				echo wp_kses_post( $args['before_title'] ) . esc_html( $title ) . wp_kses_post( $args['after_title'] );
			}
			?>
			<?php
			if ( ( $address ) ) {
				echo '<div class="contact-address">';
				echo '<span><i class="fa fa-home"></i></span>' . wp_kses_data( $address );
				echo '</div>';
			}
			?>
			<?php
			if ( ( $phone ) ) {
				echo '<div class="contact-phone">';
				echo '<span><i class="fa fa-phone"></i></span>' . wp_kses_data( $phone );
				echo '</div>';
			}
			?>
			<?php
			if ( ( $email ) ) : ?>
				<div class="contact-email">
					<span><i class="fa fa-envelope"></i></span>
					<a href="mailto:<?php echo esc_attr( $email ); ?>"><?php echo esc_html( $email ); ?></a>
				</div>
				<?php endif;
			?>
		</div><!-- /.contact_info -->

<?php
if ( isset( $args['after_widget'] ) ) {
	echo wp_kses( $args['after_widget'], 'li' );
}
if ( ! $this->is_preview() ) {
	$cache[ $args['widget_id'] ] = ob_get_flush();
		wp_cache_set( 'reendex_contact_info', $cache, 'widget' );
} else {
	ob_end_flush();
}
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']      = strip_tags( $new_instance['title'] );
		$instance['address']    = strip_tags( $new_instance['address'] );
		$instance['phone']      = strip_tags( $new_instance['phone'] );
		$instance['email']      = sanitize_email( $new_instance['email'] );
		$instance['extclass']   = sanitize_text_field( $new_instance['extclass'] );

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions['reendex_contact_info'] ) ) {
			delete_option( 'reendex_contact_info' );
		}

		return $instance;
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options.
	 */
	function form( $instance ) {
		$defaults = array(
			'extclass' 	        => '',
		);
		$title      = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$address    = isset( $instance['address'] ) ? esc_html( $instance['address'] ) : '';
		$phone      = isset( $instance['phone'] ) ? esc_html( $instance['phone'] ) : '';
		$email      = isset( $instance['email'] ) ? esc_html( $instance['email'] ) : '';
		$extclass   = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
	?>

	<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'reendex' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>

	<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php esc_html_e( 'Enter your address', 'reendex' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" size="3" />
	</p>

	<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php esc_html_e( 'Enter your phone number', 'reendex' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" size="3" />
	</p>

	<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>"><?php esc_html_e( 'Enter your email address', 'reendex' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" type="text" value="<?php echo esc_attr( sanitize_email( $email ) ); ?>" size="3" />
	</p>
	
	<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
		<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $extclass ); ?>" />
	</p>	

	<?php
	}
}
?>
