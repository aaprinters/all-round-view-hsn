<?php
/**
 * Recent Posts widget.
 *
 * @package Reendex
 */

	/**
	 * Register widget.
	 *
	 * Calls 'widgets_init' action after widget has been registered.
	 *
	 * @since 1.0.0
	 */
function register_recent_widget() {
	register_widget( 'reendex_recent_post_Widget' );
}
	add_action( 'widgets_init', 'register_recent_widget' );

	/**
	 * Core class used to implement the Recent Posts widget.
	 *
	 * @since  1.0
	 *
	 * @see WP_Widget
	 */
class Reendex_Recent_Post_Widget extends WP_Widget {
	/**
	 * Constructor.
	 */
	function __construct() {
		$widget_ops = array(
			'classname'     => 'reendex_recent',
			'description'   => esc_html__( 'All Round View: Recent Posts Widget','reendex' ),
			);
		$control_ops = array(
			'id_base' => 'reendex_recent-widget',
			);
		parent::__construct( 'reendex_recent-widget', esc_html__( 'All Round View: Recent Posts','reendex' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current Recent Posts widget instance.
	 *
	 * @param array $args     Display arguments including 'before_widget' and 'after_widget'.
	 * @param array $instance Settings for the current Recent Posts widget instance.
	 */
	function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		$number = $instance['number'];
		$show_title = isset( $instance['show_title'] ) ? $instance['show_title'] : '';
		$title_excerpt_words = isset( $instance['title_excerpt_words'] ) ? absint( $instance['title_excerpt_words'] ) : '';
		$excerpt_words = absint( $instance['excerpt_words'] );
		$categories = isset( $instance['categories'] )? (array) $instance['categories'] :array();

		if ( isset( $args['before_widget'] ) ) {
			echo wp_kses( $args['before_widget'], 'li' );
		}
		?>

		<div class="reendex_recent <?php if ( '' != 'extclass' ) { echo esc_attr( $extclass ); } ?> widget container-wrapper">
			<?php
			if ( $title ) {
				echo '<h4 class="widget-title">' . esc_html( $title ) . '</h4>';
			}
			?>
			<?php
			$args = array(
				'post_type'         => 'post',
				'posts_per_page'    => $number,
				'title_length'      => $title_excerpt_words,
				'content_length'    => $excerpt_words,
				'has_password'      => false,
				'order'             => 'DESC',
			);
			if ( is_array( $categories ) && count( $categories ) > 0 ) {
				$args['category__in'] = $categories;
			}

			$post = new WP_Query( $args );
			if ( $post->have_posts() ) :
			?>
			<ul class="recent-posts-widget">
				<?php while ( $post->have_posts() ) : $post->the_post(); ?>
				<?php
					$permalink = get_permalink();
					$image_id = get_post_thumbnail_id();
					$image_path = wp_get_attachment_image_src( $image_id, 'reendex_recent_post_thumb', true );
					$image_alt  = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
				?>
					<li>
						<div class="recent-posts">
							<div class="item">
							<?php if ( has_post_thumbnail() ) : ?>
								<div class="item-image">
									<a class="img-link" href="<?php echo esc_url( get_permalink() ); ?>">
										<img class="img-responsive img-full" src="<?php echo esc_url( $image_path[0] ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>" title="<?php the_title(); ?>" />
										<?php if ( has_post_format( 'video' ) ) : ?>
											<span class="video-icon-small">
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video-icon-small.png" alt="video"/>
											</span>
										<?php endif; ?>
									</a>
								</div><!-- /.item-image -->
							<?php endif; ?>
								<div class="item-content">
									<?php if ( 0 != $show_title ) :?>
										<h4 class="ellipsis"><a href="<?php echo esc_url( $permalink ); ?>"><?php reendex_the_title_excerpt( '', '...', true, $title_excerpt_words ); ?></a></h4>
									<?php endif;?>									    
										<p><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( reendex_read_more( get_the_excerpt(), $excerpt_words ) ); ?></a></p> 
								</div><!-- /.item-content -->
							</div><!-- /.item -->
						</div><!-- /.recent-posts -->
					</li>
				<?php endwhile;
				endif;
				wp_reset_postdata();
				?>
			</ul><!-- /.recent-posts-widget -->			
		</div><!-- /.reendex_recent -->

		<?php
		if ( isset( $args['after_widget'] ) ) {
			echo wp_kses( $args['after_widget'], 'li' );
		}
	}

	/**
	 * Handles updating the settings for the current Recent Posts widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance                           = $old_instance;
		$instance['title']                  = sanitize_text_field( $new_instance['title'] );
		$instance['number']                 = absint( $new_instance['number'] );
		$instance['show_title'] 	        = sanitize_text_field( $new_instance['show_title'] );
		$instance['title_excerpt_words']    = absint( $new_instance['title_excerpt_words'] );
		$instance['excerpt_words']          = absint( $new_instance['excerpt_words'] );
		$instance['categories']             = reendex_sanitize_multiple_checkbox( $new_instance['categories'] );
		$instance['extclass']               = sanitize_text_field( $new_instance['extclass'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Recent Posts widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$defaults = array(
			'title'                 => esc_html__( 'Recent Posts', 'reendex' ),
			'number'                => 3,
			'show_title'            => 0,
			'title_excerpt_words'   => 40,
			'excerpt_words'         => 56,
			'categories'            => array(),
			'extclass'              => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$extclass = isset( $instance['extclass'] ) ? $instance['extclass'] : '';
		$categories = $this->reendex_get_list_categories( 0 );
		if ( ! is_array( $instance['categories'] ) ) {
			$instance['categories'] = array();
		}

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of Posts to show','reendex' ); ?>:</label>
			<input class="widefat" type="number" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo esc_attr( $instance['number'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_title' ) ); ?>"><?php esc_html_e( 'Show Title', 'reendex' ); ?> </label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'show_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_title' ) ); ?>">
				<option value="1" <?php selected( '1', $instance['show_title'] ) ?>><?php esc_html_e( 'Yes','reendex' );?></option>
				<option value="0" <?php selected( '0', $instance['show_title'] ) ?>><?php esc_html_e( 'No','reendex' );?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title_excerpt_words' ) ); ?>"><?php esc_html_e( 'Title excerpt length', 'reendex' ); ?> </label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title_excerpt_words' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title_excerpt_words' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['title_excerpt_words'] ); ?>" />
		</p>		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'excerpt_words' ) ); ?>"><?php esc_html_e( 'Content excerpt length', 'reendex' ); ?> </label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'excerpt_words' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'excerpt_words' ) ); ?>" type="number" min="0" value="<?php echo esc_attr( $instance['excerpt_words'] ); ?>" />
		</p>
		<p>
			<label><?php esc_html_e( 'Select categories', 'reendex' ); ?></label> 
			<div class="categorydiv">
				<div class="tabs-panel">
					<ul class="categorychecklist">
						<?php foreach ( $categories as $cat ) { ?>
						<li>
							<label>
								<input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $cat->term_id ); ?>]" value="<?php echo esc_attr( $cat->term_id ); ?>" <?php checked( in_array( $cat->term_id, $instance['categories'] ) ); ?> />
								<?php echo esc_html( $cat->name ); ?>
							</label>
							<?php $this->reendex_get_list_sub_categories( $cat->term_id, $instance ); ?>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>"><?php esc_html_e( 'Widget area class','reendex' ); ?>:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'extclass' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'extclass' ) ); ?>" value="<?php echo esc_attr( $instance['extclass'] ); ?>" />
		</p>		
	<?php
	}
	/**
	 * Returns a list of categories.
	 *
	 * @param string $cat_parent_id The parent term ID.
	 */
	function reendex_get_list_categories( $cat_parent_id ) {
		$args = array(
				'hierarchical'  => 1,
				'parent'		=> $cat_parent_id,
				'title_li'		=> '',
				'child_of'		=> 0,
			);
		$cats = get_categories( $args );
		return $cats;
	}

	/**
	 * Returns a list of subcategories.
	 *
	 * @param string $cat_parent_id The parent term ID.
	 * @param array  $instance Settings for the current Categories widget instance.
	 */
	function reendex_get_list_sub_categories( $cat_parent_id, $instance ) {
		$sub_categories = $this->reendex_get_list_categories( $cat_parent_id );
		if ( count( $sub_categories ) > 0 ) {
		?>
			<ul class="children">
				<?php foreach ( $sub_categories as $sub_cat ) { ?>
					<li>
						<label>
							<input type="checkbox" name="<?php echo esc_attr( $this->get_field_name( 'categories' ) ); ?>[<?php esc_attr( $sub_cat->term_id ); ?>]" value="<?php echo esc_attr( $sub_cat->term_id ); ?>" <?php checked( in_array( $sub_cat->term_id,$instance['categories'] ) ); ?> />
							<?php echo esc_html( $sub_cat->name ); ?>
						</label>
						<?php $this->reendex_get_list_sub_categories( $sub_cat->term_id, $instance ); ?>
					</li>
				<?php } ?>
			</ul>
		<?php }
	}
}
