<?php
/**
 * Functions for handling theme custom styles in the frontend.
 *
 * @package Reendex
 */

/**
 * Add dynamic css styles
 */
function reendex_dynamic_styles() {

	$color_scheme_1 = get_theme_mod( 'reendex_theme_color', '#d4000e' );
	$color_scheme_2 = get_theme_mod( 'reendex_theme_secondary_color', '#be000c' );
	$hover_background_color = get_theme_mod( 'reendex_hover_background_color', '#e4000d' );
	$hover_color = get_theme_mod( 'reendex_hover_color', '#fff' );
	$body_font = get_theme_mod( 'reendex_body_font_family', 'Roboto' );
	$title_font_option = get_theme_mod( 'reendex_title_font_option', 'Roboto Condensed' );
	$body_font_size = get_theme_mod( 'reendex_body_font_size', '14' );
	$first_nav_font_size = get_theme_mod( 'reendex_first_nav_font_size', '14' );
	$first_nav_font_weight = get_theme_mod( 'reendex_first_nav_font_weight', '500' );
	$bottom_nav_font_size = get_theme_mod( 'reendex_bottom_nav_font_size', '14' );
	$bottom_nav_font_weight = get_theme_mod( 'reendex_bottom_nav_font_weight', '400' );
	$preloader_bg = get_theme_mod( 'reendex_preloader_bg', '#fff' );
	$preloader_color = get_theme_mod( 'reendex_preloader_color', '#777' );
	$banner_title_text_color = get_theme_mod( 'reendex_banner_title_text_color', '#fff' );
	$banner_breadcrumbs_bg_color = get_theme_mod( 'reendex_banner_breadcrumbs_bg_color', '#000' );
	$banner_breadcrumbs_text_color = get_theme_mod( 'reendex_banner_breadcrumbs_text_color', '#fff' );
	$banner_single_title_text_color = get_theme_mod( 'reendex_single_banner_title_text_color', '#fff' );
	$banner_single_breadcrumbs_bg_color = get_theme_mod( 'reendex_single_banner_breadcrumbs_bg_color', '#000' );
	$banner_single_breadcrumbs_text_color = get_theme_mod( 'reendex_single_banner_breadcrumbs_text_color', '#fff' );
	$banner_blog_title_text_color = get_theme_mod( 'reendex_blog_banner_title_text_color', '#fff' );
	$banner_blog_subtitle_bg_color = get_theme_mod( 'reendex_blog_banner_subtitle_bg_color', '#000' );
	$banner_blog_subtitle_text_color = get_theme_mod( 'reendex_blog_banner_subtitle_text_color', '#fff' );
	$banner_contact_title_text_color = get_theme_mod( 'reendex_contact_banner_title_text_color', '#fff' );
	$banner_contact_subtitle_bg_color = get_theme_mod( 'reendex_contact_banner_subtitle_bg_color', '#000' );
	$banner_contact_subtitle_text_color = get_theme_mod( 'reendex_contact_banner_subtitle_text_color', '#fff' );
	$custom_css = '';

		$custom_css .= "
			.title-style01 > h3, .style-02 span, .nav-tabs.nav-tabs-bottom,
			.comment-title h4 {
				border-bottom: 2px solid {$color_scheme_1};
			}";
		$custom_css .= "
			.main-menu, .search-icon-btn, .breaking-ribbon, .flexslider li h3, #sidebar-newsletter button, #newsletter button, #time, .btn-primary:hover, .sponsored, .sponsored-big, .play-icon, .title-style05.style-01, .title-left.underline04::before, .title-left.underline03::before, #footer .widget-title::before, .mega-menu-wrapper .widget-title::before, .page-numbers  span.current, .read-more:hover:before, #nav-below-main ul li a:hover:before, #nav-below-main ul li.current-menu-item a:before, .btn:hover, .btn-info.focus, .btn-info:focus, .btn-default, .weather-city, .calendar_wrap caption, .icon-sub-menu .up-icon, .tvbanner-area .bg-1, .widget-search .search-submit, .block-title-1, #wprmenu_menu .wprmenu_icon_par.wprmenu_par_opened, .archive-label, .bg-1, .header-search-box, .sidebar-weather.widget .widget-title, .pagination .nav-links span.current, .pagination .nav-links a:hover, .block-title-2, .dropdown-menu > .active > a, .dropdown-menu > .active > a:focus, .dropdown-menu > .active > a:hover, .nav-previous a:hover::before, .nav-next a:hover::after, .related-post-cat a, .page-links span, .section-highlighs-wrapper span a, .section-header h2::after, .section-highlighs-wrapper span a::before, .contact-formarea input, #sidebar-newsletter .sidebar-newsletter-inner h5, #sidebar-newsletter .sidebar-newsletter-inner h5::before, .sidebar-social-icons .sidebar-social-icons-inner h5::before, .twitter-feed-area .twitter-feed-inner h5, .twitter-feed-area .twitter-feed-inner h5::before, .container-fluid.header-style-five .main-menu ul.mega-main-menu::before, .select2-container--default .select2-results__option--highlighted[aria-selected], .logo-gallery-header h3::after, .video-promo-item .section-highlight-inner::before, .promo-item .section-highlight-inner a::before, .promo-item .section-highlight-inner a, .video-promo-item .section-highlight-inner, .single input[type=submit]:hover, .live-updates .live-title, .rtl .btn-default {
				background-color: {$color_scheme_1};
			}";
		$custom_css .= "
			.title-style02, .title-style02-light, .blockquote_style02, h3.article-title-left, .post-style-default .entry-header .post-meta-elements, .post-style-7 .entry-header .post-meta-elements, .instagram-content .instagram-subtitle {
				border-left: 3px solid {$color_scheme_1};
			}";
		$custom_css .= "
			.rtl .title-style02, .rtl .title-style02-light, .rtl .blockquote_style02, .rtl h3.article-title-left, .rtl .post-style-default .entry-header .post-meta-elements, .rtl .post-style-7 .entry-header .post-meta-elements, .rtl .instagram-content .instagram-subtitle {
				border-right: 3px solid {$color_scheme_1};
				border-left: none;
			}";
		$custom_css .= "
			a, .contact-us a, .content-wrap a, #calendar .ui-datepicker-calendar tbody td a.ui-state-highlight, #nav-below-main ul li a:hover, #nav-below-main ul li.current-menu-item a, a:hover, .hour, .date, .day, .time, .header-logo h1, .dropdown-menu a:hover, .read-more:hover, .blockquote_style01::before, .contact-us i, .currency h4, .square10, .square11, #cdate, .contact-email a, .entry-footer span a, .local-weather-title, .post .entry-content p a, .dot {
				color: {$color_scheme_1};
			}";
		$custom_css .= "
			.page-numbers span.current {
				border: 1px solid {$color_scheme_1};
			}";
		$custom_css .= "
			.news-gallery-slider .post-content > a {
				border-left: 2px solid {$color_scheme_1} !important;
			}";
		$custom_css .= "
			.rtl .news-gallery-slider .post-content > a {
				border-right: 2px solid {$color_scheme_1} !important;
				border-left: none !important;
			}";
		$custom_css .= "
			.post-style-2 blockquote {
				border-left: 4px solid {$color_scheme_1} !important;
			}";
		$custom_css .= "
			.rtl .post-style-2 blockquote {
				border-right: 4px solid {$color_scheme_1} !important;
				border-left: none !important;
			}";
		$custom_css .= "
			.post-style-3 .entry-header .entry-title, .post-style-4 .entry-header .header-subtitle, post-style-2 .entry-header .header-subtitle, .section-header-left .section-subtitle, .post-style-2 .entry-header .header-subtitle, .post-style-4 .entry-header .header-subtitle {
				border-left: 6px solid {$color_scheme_1} !important;
			}";
		$custom_css .= "
			.rtl .post-style-3 .entry-header .entry-title, .rtl .post-style-4 .entry-header .header-subtitle, .rtl post-style-2 .entry-header .header-subtitle, .rtl .section-header-left .section-subtitle, .rtl .post-style-2 .entry-header .header-subtitle, .rtl .post-style-4 .entry-header .header-subtitle {
				border-right: 6px solid {$color_scheme_1} !important;
				border-left: none !important;
			}";
		$custom_css .= "
			.section-highlighs, .sidebar-newsletter-form, .twitter-feed-wrapper, .video-promo-content, .promo-content {
				border-top: 6px solid {$color_scheme_1} !important;
			}";
		$custom_css .= "
			.main_menu ul > li.current-menu-item > a, .main_menu ul > li.current-page-ancestor > a {
				background-color: {$color_scheme_2} !important;
			}";
		$custom_css .= "
			.sponsored:hover, .sponsored-big:hover, #sidebar-newsletter button:hover, .nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .carousel-title:hover, .pagination > .active > a:hover, .btn-default:hover, .nav-tabs.nav-tabs-solid > .active > a:focus, .nav-tabs.nav-tabs-solid .active a, .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover, .dropdown-menu > li > a:hover {
				background-color: {$hover_background_color} !important;
			}";
		$custom_css .= "
			.main_menu ul>li>a:focus, .main_menu ul>li>a:hover, .contact-formarea input:hover {
				background-color: {$hover_background_color};
			}";
		$custom_css .= "
			.item .item-content a:hover, .item-content p a:hover, .events p a:hover, .small-gallery .post-content p a:hover, .sidebar-block .sidebar-content a:hover, .sidebar-post.light p a:hover, .single-related-posts .entry-title a:hover, .post-meta-author a:hover, .single-related-posts .content a:hover, .external-link p:hover, .container-full .item-content h4 a:hover, .promo-item .promo-content .read-more:hover, .video-promo-item .video-promo-content .read-more:hover, .sidebar-post.light .item-content p a:hover {
				color: {$hover_background_color} !important;
			}";
		$custom_css .= "
			.sponsored:hover, .sponsored-big:hover, #sidebar-newsletter button:hover, .main-menu .main_menu ul>li>a:hover, .nav .open>a, .nav .open>a:focus, .nav .open>a:hover, .carousel-title:hover, .pagination > .active > a:hover, .btn-default:hover, .nav-tabs.nav-tabs-solid > .active > a:focus, .nav-tabs.nav-tabs-solid .active a, .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover, .dropdown-menu > li > a:hover, .block-title-1, .block-title-2, .main_menu ul>li>a:focus, .main_menu ul>li>a:hover {
				color: {$hover_color};
			}";
		$custom_css .= "
			body, h1, h2, h4, h5, h6, h1 a, h2 a, h4 a, h5 a, h6 a {
				font-family: {$body_font}, sans-serif;
			}";
		$custom_css .= "
			h3 {
				font-family: {$title_font_option}, serif;
			}";
		$custom_css .= "
			body {
				font-size: {$body_font_size}px;
			}";
		$custom_css .= "
			.main_menu ul > li > a {
				font-size: {$first_nav_font_size}px;
				font-weight: {$first_nav_font_weight};
			}";
		$custom_css .= "
			#nav-below-main ul li a {
				font-size: {$bottom_nav_font_size}px;
				font-weight: {$bottom_nav_font_weight};
			}";
		$custom_css .= "
			.pageloader {
				background-color: {$preloader_bg};
			}";
		$custom_css .= "
			.spinner > div {
				background-color: {$preloader_color};
			}";
		$custom_css .= "
			h1.page-title span {
				color: {$banner_title_text_color};
			}";
		$custom_css .= "
			.archive .breadcrumb,
            .search .breadcrumb,
            .page-template-default .breadcrumb {
				background-color: {$banner_breadcrumbs_bg_color};
			}";
		$custom_css .= "
			.archive .breadcrumb span,
            .search .breadcrumb span,
            .archive .breadcrumb a,
            .search .breadcrumb a,
            .page-template-default .breadcrumb span,
            .page-template-default .breadcrumb a {
				color: {$banner_breadcrumbs_text_color} !important;
			}";
		$custom_css .= "
			.single h1.page-title span {
				color: {$banner_single_title_text_color};
			}";
		$custom_css .= "
			.single .breadcrumb {
				background-color: {$banner_single_breadcrumbs_bg_color};
			}";
		$custom_css .= "
			.single .breadcrumb span,
            .single .breadcrumb a {
				color: {$banner_single_breadcrumbs_text_color} !important;
			}";
		$custom_css .= "
			.blog h1.page-title span {
				color: {$banner_blog_title_text_color};
			}";
		$custom_css .= "
			.blog .page-subtitle {
				background-color: {$banner_blog_subtitle_bg_color};
			}";
		$custom_css .= "
			.blog .page-subtitle span a {
				color: {$banner_blog_subtitle_text_color} !important;
			}";
		$custom_css .= "
			.contact-page-header h1.page-title span {
				color: {$banner_contact_title_text_color};
			}";
		$custom_css .= "
			.contact-page-header .page-subtitle {
				background-color: {$banner_contact_subtitle_bg_color};
			}";
		$custom_css .= "
			.contact-page-header .page-subtitle span a {
				color: {$banner_contact_subtitle_text_color} !important;
			}";
	?>
	<?php
	wp_add_inline_style( 'reendex-style', $custom_css );
}

add_action( 'wp_enqueue_scripts','reendex_dynamic_styles' );
