<?php
/**
 * Responsive menu
 *
 * @package Reendex
 */

/**
 * Apply style to responsive menu.
 */
function reendex_header_styles() {
		$reendex_rm_bar_bgd = get_theme_mod( 'reendex_rm_bar_bgd', '#fcfcfc' );
		$reendex_rm_bar_opacity = get_theme_mod( 'reendex_rm_bar_opacity', '0.9' );
		$reendex_rm_bar_color = get_theme_mod( 'reendex_rm_bar_color', '#777' );
		$reendex_rm_menu_bgd = get_theme_mod( 'reendex_rm_menu_bgd', '#2c2c34' );
		$reendex_rm_menu_opacity = get_theme_mod( 'reendex_rm_menu_opacity', '0.98' );
		$reendex_rm_menu_border_bottom = get_theme_mod( 'reendex_rm_menu_border_bottom', '#55555b' );
		$reendex_rm_menu_border_top = get_theme_mod( 'reendex_rm_menu_border_top', '#55555b' );
		$reendex_rm_menu_color = get_theme_mod( 'reendex_rm_menu_color', '#b5b5b5' );
		$reendex_rm_menu_color_hover = get_theme_mod( 'reendex_rm_menu_color_hover', '#fff' );
		$reendex_rm_menu_icon_color = get_theme_mod( 'reendex_rm_menu_icon_color', '#777' );
		$reendex_rm_how_wide = get_theme_mod( 'reendex_rm_how_wide', '100' );
		$reendex_rm_menu_symbol_pos = get_theme_mod( 'reendex_rm_menu_symbol_pos', 'right' );
		$custom_css = '';

		$custom_css .= "
			#mobile-nav-wrapper {
				background-color: {$reendex_rm_bar_bgd};
				opacity: {$reendex_rm_bar_opacity}!important;
			}";
		$custom_css .= "
		   #wprmenu_bar .wprmenu_icon span {
				background-color: {$reendex_rm_bar_color}!important;
			}";
		$custom_css .= "
			#wprmenu_menu {
				background-color: {$reendex_rm_menu_bgd}!important;
				opacity: {$reendex_rm_menu_opacity}!important;
			}";
		$custom_css .= "
			#wprmenu_menu.wprmenu_levels ul li {
				border-bottom: 1px solid {$reendex_rm_menu_border_bottom};
				border-top: 1px solid {$reendex_rm_menu_border_top};
			}";
		$custom_css .= "
			#wprmenu_menu.wprmenu_levels ul li ul {
				border-top:1px solid {$reendex_rm_menu_border_bottom};
			}";
		$custom_css .= "
			#wprmenu_menu ul li a, #wprmenu_menu .wprmenu_icon_par {
				color: {$reendex_rm_menu_color};
			}";
		$custom_css .= "
			#wprmenu_menu ul li a:hover, #wprmenu_menu .wprmenu_icon_par:hover {
				color: {$reendex_rm_menu_color_hover};
			}";
		$custom_css .= "
			#wprmenu_menu.wprmenu_levels a.wprmenu_parent_item {
				border-left: 1px solid {$reendex_rm_menu_border_top};
			}";
	if ( is_rtl() ) :
		$custom_css .= "
			#wprmenu_menu.wprmenu_levels a.wprmenu_parent_item {
				border-right: 1px solid {$reendex_rm_menu_border_top};
			}";
	endif;
	$custom_css .= '
		.menu-toggle {
			display: none!important;
		}';
	$custom_css .= '
		@media (max-width: 1100px) {
			.menu-toggle,.topsearch {
				display: none!important;
			}				
		}';

		// when hide bottom borders option is on...
	if ( get_theme_mod( 'reendex_rm_menu_border_bottom_show', 'no' ) === 'no' ) :
		$custom_css .= '
			#wprmenu_menu, #wprmenu_menu ul, #wprmenu_menu li {
				border-bottom:none!important;
			}';
		$custom_css .= '
			#wprmenu_menu, #wprmenu_menu ul, #wprmenu_menu li {
				border-bottom:none!important;
			}';
		$custom_css .= "
			#wprmenu_menu.wprmenu_levels > ul {
				border-bottom:1px solid {$reendex_rm_menu_border_top}!important;
			}";
		$custom_css .= '
			.wprmenu_no_border_bottom {
				border-bottom:none!important;
			}';
		$custom_css .= '
			#wprmenu_menu.wprmenu_levels ul li ul {
				border-top:none!important;
			}';
	endif;

	$custom_css .= "
		#wprmenu_menu.left {
			width: {$reendex_rm_how_wide}%;
			left: -100%;
			right: auto;
		}";
	$custom_css .= "
		#wprmenu_menu.right {
			width: {$reendex_rm_how_wide}%;
			right: -100%;
			left: auto;
		}";
	$custom_css .= '
		#wprmenu_menu input.search-field {
			padding: 6px 6px;
			background-color: #999;
			color: #333;
			border: #666;
			margin: 6px 6px;
		}';
	$custom_css .= '
		#wprmenu_menu input.search-field:focus {
			background-color: #CCC;
			color: #000;
		}';

	if ( get_theme_mod( 'reendex_rm_menu_symbol_pos', 'right' ) == 'left' ) :
		$custom_css .= "
			#wprmenu_bar {
				float: {$reendex_rm_menu_symbol_pos}!important;
				margin: 30px 15px 0 15px!important;
			}
			#wprmenu_bar .wprmenu_icon {
				float: {$reendex_rm_menu_symbol_pos}!important;
			}";
		$custom_css .= '
			.mobile-menu-logo {
				padding-left: 0px!important;
			}';
	endif;
	if ( is_rtl() && get_theme_mod( 'reendex_rm_menu_symbol_pos', 'left' ) == 'right' ) :
		$custom_css .= "
			#wprmenu_bar {
				float: {$reendex_rm_menu_symbol_pos}!important;
				margin: 30px 15px 0 15px!important;
			}
			#wprmenu_bar .wprmenu_icon {
				float: {$reendex_rm_menu_symbol_pos}!important;
			}";
		$custom_css .= '
			.mobile-menu-logo {
				padding-right: 0px!important;
			}';
	endif;

		$custom_css .= '
			@media screen and (max-width: 1100px) {
				div#wpadminbar { position: fixed; }
				#wpadminbar + #wprmenu_menu.left { top: 105px; }
		}';
?>

		<?php
		wp_add_inline_style( 'reendex-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'reendex_header_styles' );
