<?php
/**
 * Customizer sanitization
 *
 * Handles santization of theme customizer options.
 *
 * @package Reendex
 * @since 1.0
 */

/**
 * Number Sanitization.
 *
 * @param string $input Input data.
 *
 * @return string
 */
function reendex_sanitize_number( $input ) {
	if ( isset( $input ) && is_numeric( $input ) ) {
		return $input;
	}
}

/**
 * Checkbox Sanitization.
 *
 * @since 1.0.0
 *
 * @param bool $checked Whether the checkbox is checked.
 * @return bool Whether the checkbox is checked.
 */
function reendex_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true === $checked ) ? true : false );
}

/**
 * Date Sanitization.
 *
 * @since 1.0
 *
 * @param string $input Input string.
 */
function reendex_sanitize_date( $input ) {
	$date = new DateTime( $input );
	return $date->format( 'm/d/Y' );
}

/**
 * No-HTML Sanitization.
 *
 * @since 1.0
 *
 * @param string $nohtml The no-HTML content to sanitize.
 * @return string Sanitized no-HTML content.
 */
function reendex_sanitize_nohtml( $nohtml ) {
	return wp_filter_nohtml_kses( $nohtml );
}

/**
 * HTML Sanitization.
 *
 * @since 1.0
 *
 * @param string $html HTML to sanitize.
 * @return string Sanitized HTML.
 */
function reendex_sanitize_html( $html ) {
	return wp_filter_post_kses( $html );
}

/**
 * URL sanitation allowing unicode characters
 *
 * @param string $url URL to sanitize.
 * @return string Sanitized URL.
 */
function reendex_sanitize_url( $url ) {
	return esc_url_raw( $url );
}

/**
 * Textarea Sanitization.
 *
 * @since 1.0
 *
 * @param string               $input Content to be sanitized.
 * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
 * @return string Sanitized content.
 */
function reendex_sanitize_textarea( $input, $setting ) {
	return wp_kses_post( $input );
}

/**
 * Filters a sanitized text field string.
 *
 * @since 2.9.0
 *
 * @param string $str String to sanitize.
 * @return string Sanitized string.
 */
function reendex_sanitize_text_field( $str ) {
	$filtered = _sanitize_text_fields( $str, false );
	/**
	 * Filters a sanitized text field string.
	 *
	 * @since 2.9.0
	 *
	 * @param string $filtered The sanitized string.
	 * @param string $str      The string prior to being sanitized.
	 */
	return apply_filters( 'reendex_sanitize_text_field', $filtered, $str );
}

/**
 * Hex color Sanitization.
 *
 * @param string $color    The proposed color.
 * @return string    The sanitized color.
 */
function reendex_sanitize_hex_color( $color ) {
	if ( '' === $color ) {
		return '';
	}

	// 3 or 6 hex digits, or the empty string.
	if ( preg_match( '|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) ) {
		return $color;
	}
}

/**
 * Multiple checkbox values Sanitization.
 *
 * @param string $values Values.
 * @return array Checked values.
 */
function reendex_sanitize_multiple_checkbox( $values ) {
	$multi_values = ! is_array( $values ) ? explode( ',', $values ) : $values;
		return ! empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();
}

/**
 * Sanitize select, radio.
 *
 * @since 1.0
 *
 * @param mixed                $input The value to sanitize.
 * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
 * @return mixed Sanitized value.
 */
function reendex_sanitize_select( $input, $setting ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Radio Button sanitization callback.
 *
 * @see sanitize_key() https://developer.wordpress.org/reference/functions/sanitize_key/
 *
 * @param String $input Slug to sanitize.
 */
function reendex_related_posts_sanitize( $input ) {
	$valid_keys = array(
		'categories' => esc_html__( 'Related Posts By Categories', 'reendex' ),
		'tags'       => esc_html__( 'Related Posts By Tags', 'reendex' ),
	);
	if ( array_key_exists( $input, $valid_keys ) ) {
		return $input;
	} else {
		return '';
	}
}

/**
 * Custom sanitisation callback - Switch.
 *
 * @param string $input Current input value.
 */
function reendex_customizer_callback_sanitize_switch( $input ) {
	$valid_keys = array(
			'enable'   => esc_html__( 'Enable', 'reendex' ),
			'disable'  => esc_html__( 'Disable', 'reendex' ),
		);
	if ( array_key_exists( $input, $valid_keys ) ) {
		return $input;
	} else {
		return '';
	}
}

if ( ! function_exists( 'reendex_date_format' ) ) :
	/**
	 * Check if Shortcode Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_date_format( $control ) {

		if ( 'custom' === $control->manager->get_setting( 'reendex_post_date_format' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

/**
 * Banner sanitisation callback.
 */
function reendex_banner_allowed_html() {
	return array(
		'a' => array(
			'href' => array(),
			'rel' => array(),
			'class' => array(),
		),
		'img' => array(
			'src'    => array(),
			'alt'    => array(),
			'height' => array(),
			'width'  => array(),
			'class'  => array(),
		),
		'div' => array(
			'class' => array(),
			'style' => array(),
		),
		'strong' => array(
			'class' => array(),
		),
		'em' => array(
			'class' => array(),
		),
		'script' => array(
			'async'        => array(),
			'src'          => array(),
			'type'         => array(),
			'data-id'      => array(),
			'data-format'  => array(),
		),
		'ins' => array(
			'class'          => array(),
			'style'          => array(),
			'data-ad-client' => array(),
			'data-ad-slot'   => array(),
			'data-ad-format' => array(),
		),
		'style' => array(
			'media' => true,
			'scoped' => true,
			'type' => true,
		),
	);
}

/**
 * Styles sanitization callback.
 *
 * @param string $styles Styles to sanitize.
 * @return string sanitized Styles.
 */
function reendex_modify_safe_css( $styles ) {
	$styles[] = 'display';
	return $styles;
}

/**
 * Sanitize Banner HTML.
 *
 * @param  string $html The HTML result.
 * @return string sanitized html code.
 */
function reendex_sanitize_banner( $html ) {
	add_filter( 'safe_style_css', 'reendex_modify_safe_css' );
	$html = wp_kses( $html, reendex_banner_allowed_html() );
	return $html;
}

/**
 * Callback functions for active_callback.
 */
if ( ! function_exists( 'reendex_header_ad_type_image' ) ) :
	/**
	 * Check if Image Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_header_ad_type_image( $control ) {

		if ( 'image' === $control->manager->get_setting( 'reendex_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_header_ad_type_code' ) ) :
	/**
	 * Check if Script Code type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_header_ad_type_code( $control ) {

		if ( 'code' === $control->manager->get_setting( 'reendex_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_header_ad_type_google' ) ) :
	/**
	 * Check if Google Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_header_ad_type_google( $control ) {

		if ( 'googleads' === $control->manager->get_setting( 'reendex_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_header_ad_type_shortocde' ) ) :
	/**
	 * Check if Shortcode Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_header_ad_type_shortocde( $control ) {

		if ( 'shortcode' === $control->manager->get_setting( 'reendex_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_above_header_ad_type_image' ) ) :
	/**
	 * Check if Image Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_above_header_ad_type_image( $control ) {

		if ( 'image' === $control->manager->get_setting( 'reendex_above_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_above_header_ad_type_code' ) ) :
	/**
	 * Check if Script Code type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_above_header_ad_type_code( $control ) {

		if ( 'code' === $control->manager->get_setting( 'reendex_above_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_above_header_ad_type_google' ) ) :
	/**
	 * Check if Google Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_above_header_ad_type_google( $control ) {

		if ( 'googleads' === $control->manager->get_setting( 'reendex_above_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_above_header_ad_type_shortocde' ) ) :
	/**
	 * Check if Shortcode Ad type is active.
	 *
	 * @param WP_Customize_Control $control WP_Customize_Control instance.
	 *
	 * @return bool Whether the control is active to the current preview.
	 */
	function reendex_above_header_ad_type_shortocde( $control ) {

		if ( 'shortcode' === $control->manager->get_setting( 'reendex_above_header_ad_type' )->value() ) {
			return true;
		} else {
			return false;
		}
	}
endif;

if ( ! function_exists( 'reendex_sanitize_allowed_tags' ) ) {
	/**
	 * Sanitize text with allowed html.
	 *
	 * @param string $value The unsanitized string.
	 * @return string The sanitized value.
	 */
	function reendex_sanitize_allowed_tags( $value ) {
		return wp_kses( $value, wp_kses_allowed_html() );
	}
}
