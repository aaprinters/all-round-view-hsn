<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Reendex
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="error-404 container-fluid text-center">
				<h1 class="extra-large-caption"><?php esc_html_e( 'Error 404','reendex' );?></h1>
				<h2 class="medium-caption"><?php esc_html_e( 'Page Not Found!','reendex' );?></h2>
				<h4><?php esc_html_e( 'Sorry! We could not found the page you are looking for! Please search below...','reendex' );?></h4>
				<div class="text-center">
					<div class="widget-search">
						<form role="search" method="get" class="search-form main-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<label>
								<input type="search" class="search-field" placeholder="<?php esc_attr_e( 'Search...', 'reendex' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
							</label>
							<input type="submit" class="search-submit" value="<?php esc_attr_e( 'Search', 'reendex' ); ?>">
						</form>	
					</div><!-- /.widget-search -->				
				</div><!-- /.text-center -->
				<div>
					<p class="text-center"><a class="btn btn-default btn-lg" href="<?php echo esc_url( home_url( '/' ) );?>"><?php esc_html_e( 'Take me home','reendex' );?></a></p>
				</div>
			</div><!-- /end .error 404 -->
		</main><!-- /#main -->
	</div><!-- /#primary -->

<?php
get_footer();
