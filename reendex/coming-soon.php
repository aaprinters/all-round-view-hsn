<?php
/**
 * Comming Soon Page.
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
$reendex_comingsoon_date = $options['reendex_comingsoon_date'];
?>

<div id="main-section">
	<section class="module">
		<div class="container-fluid">
			<h2 class="intro-title text-center medium-caption"><?php esc_html_e( 'We will be back soon!','reendex' );?></h2>
			<div class="countdown styled extra-large-caption" data-countdown="<?php echo esc_attr( $reendex_comingsoon_date ); ?>"></div>
			<h2 class="intro-subtitle text-center extra-small-caption"><?php esc_html_e( 'We are excited to announce that we are currently in the process of launching a new website.','reendex' );?></h2>
			<div class="container text-center"> 
				<div class="newsletter-comming mt-30">
					<div class="newsletter-form">
						<?php
							$reendex_newsletter = get_option( 'reendex_newsletter' );
						echo do_shortcode( $reendex_newsletter );
						?>
					</div><!-- /.newsletter-form -->
				</div><!-- /.newsletter-comming -->
				<?php
					$show_social = get_theme_mod( 'reendex_comingsoon_socialmedia_show', 'enable' );
					$show_social_title = get_theme_mod( 'reendex_comingsoon_socialmedia_title_show', 'enable' );
					$social_title = $options['reendex_comingsoon_socialmedia_title'];
				if ( 'disable' !== $show_social ) :
				?>
				<div class="sidebar-social-icons mt-30">
					<?php if ( 'disable' !== $show_social_title ) : ?>
						<div class="sidebar-social-icons-inner"> 
							<h5><?php echo esc_html( $social_title ); ?></h5> 
						</div>                                     
					<?php endif; ?>
					<div class="sidebar-social-icons-wrapper">
						<ul>
						<?php
						$facebook = $options['reendex_facebook_link'];
						$twitter = $options['reendex_twitter_link'];
						$g = $options['reendex_googleplus_link'];
						$linkedin = $options['reendex_linkedin_link'];
						$pinterest = $options['reendex_pinterest_link'];
						$tumblr = $options['reendex_tumblr_link'];
						$reddit = $options['reendex_reddit_link'];
						$stumbleupon = $options['reendex_stumbleupon_link'];
						$digg = $options['reendex_digg_link'];
						$vimeo = $options['reendex_vimeo_link'];
						$youtube = $options['reendex_youtube_link'];
						$rss = $options['reendex_rss_link'];
						if ( strlen( $facebook ) > 0 ) {
							echo '<li class="facebook"><a class="facebook" href="' . esc_url( $facebook ) . '"><i class="fa fa-facebook"></i></a></li>';
						}
						if ( strlen( $twitter ) > 0 ) {
							echo '<li class="twitter"><a class="twitter" href="' . esc_url( $twitter ) . '"><i class="fa fa-twitter"></i></a></li>';
						}
						if ( strlen( $g ) > 0 ) {
							echo '<li class="gplus"><a class="google-plus" href="' . esc_url( $g ) . '"><i class="fa fa-google-plus"></i></a></li>';
						}
						if ( strlen( $linkedin ) > 0 ) {
							echo '<li class="linkedin"><a class="linkedin" href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a></li>';
						}
						if ( strlen( $pinterest ) > 0 ) {
							echo '<li class="pinterest"><a class="pinterest" href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
						}
						if ( strlen( $tumblr ) > 0 ) {
							echo '<li class="tumblr"><a class="tumblr" href="' . esc_url( $tumblr ) . '"><i class="fa fa-tumblr"></i></a></li>';
						}
						if ( strlen( $reddit ) > 0 ) {
							echo '<li class="reddit"><a class="reddit" href="' . esc_url( $reddit ) . '"><i class="fa fa-reddit"></i></a></li>';
						}
						if ( strlen( $stumbleupon ) > 0 ) {
							echo '<li class="stumbleupon"><a class="stumbleupon" href="' . esc_url( $stumbleupon ) . '"><i class="fa fa-stumbleupon"></i></a></li>';
						}
						if ( strlen( $digg ) > 0 ) {
							echo '<li class="digg"><a class="digg" href="' . esc_url( $digg ) . '"><i class="fa fa-digg"></i></a></li>';
						}
						if ( strlen( $vimeo ) > 0 ) {
							echo '<li class="vimeo"><a class="vimeo" href="' . esc_url( $vimeo ) . '"><i class="fa fa-vimeo-square"></i></a></li>';
						}
						if ( strlen( $youtube ) > 0 ) {
							echo '<li class="youtube"><a class="youtube" href="' . esc_url( $youtube ) . '"><i class="fa fa-youtube"></i></a></li>';
						}
						if ( strlen( $rss ) > 0 ) {
							echo '<li class="rss"><a class="rss" href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a></li>';
						}
						?>
						</ul><!-- /ul --> 
					</div><!-- /.class="sidebar-social-icons-wrapper --> 
				</div><!-- /.sidebar-social-icons --> 
				<?php endif;?>
			</div><!-- /.container text-center -->
		</div><!-- /.container-fluid -->
	</section><!-- /.module --> 
</div><!-- /#main-section -->

<?php get_footer(); // Loads the footer.php template.
