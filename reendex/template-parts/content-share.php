<?php
/**
 * Template part for displaying Sharing buttons for post.
 *
 * @package Reendex
 */

$reendex_id = get_the_ID();
$reendex_share_facebook = get_post_meta( $reendex_id, 'reendex_share_facebook', true );
$reendex_share_twitter = get_post_meta( $reendex_id, 'reendex_share_twitter', true );
$reendex_share_google = get_post_meta( $reendex_id, 'reendex_share_google', true );
$reendex_share_linkedin = get_post_meta( $reendex_id, 'reendex_share_linkedin', true );
$reendex_share_pinterest = get_post_meta( $reendex_id, 'reendex_share_pinterest', true );
$reendex_share_tumblr = get_post_meta( $reendex_id, 'reendex_share_tumblr', true );
$reendex_share_reddit = get_post_meta( $reendex_id, 'reendex_share_reddit', true );
$reendex_share_stumbleupon = get_post_meta( $reendex_id, 'reendex_share_stumbleupon', true );
$reendex_share_digg = get_post_meta( $reendex_id, 'reendex_share_digg', true );
$reendex_share_vk = get_post_meta( $reendex_id, 'reendex_share_vk', true );
$reendex_share_pocket = get_post_meta( $reendex_id, 'reendex_share_pocket', true );
?>

<div class="share-buttons">
	<?php
	if ( ( 'show' === $reendex_share_facebook ) && ( 'disable' === get_theme_mod( 'reendex_share_facebook' ) ) || ! ( 'hide' === $reendex_share_facebook ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_facebook' ) ) ) {
		echo '<a class="facebook-share-btn btn share-button" href="' . esc_url( 'https://www.facebook.com/sharer.php?u=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Facebook', 'reendex' ) . '" target="_blank"><i class="fa fa-facebook"></i><span class="social-share-text">' . esc_html__( 'Share on Facebook', 'reendex' ) . '</span></a>';
	}
	if ( ( 'show' === $reendex_share_twitter ) && ( 'disable' === get_theme_mod( 'reendex_share_twitter' ) ) || ! ( 'hide' === $reendex_share_twitter ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_twitter' ) ) ) {
		echo '<a class="twitter-share-btn btn share-button" href="' . esc_url( 'https://twitter.com/share?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Twitter', 'reendex' ) . '" target="_blank"><i class="fa fa-twitter"></i><span class="social-share-text">' . esc_html__( 'Tweet this!', 'reendex' ) . '</span></a>';
	}
	if ( ( 'show' === $reendex_share_google ) && ( 'disable' === get_theme_mod( 'reendex_share_google' ) ) || ! ( 'hide' === $reendex_share_google ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_google' ) ) ) {
		echo '<a class="google-plus-share-btn btn share-button" href="' . esc_url( 'https://plus.google.com/share?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Google Plus', 'reendex' ) . '" target="_blank"><i class="fa fa-google-plus"></i></a>';
	}
	if ( ( 'show' === $reendex_share_linkedin ) && ( 'disable' === get_theme_mod( 'reendex_share_linkedin' ) ) || ! ( 'hide' === $reendex_share_linkedin ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_linkedin' ) ) ) {
		echo '<a class="linkedin-share-btn btn share-button" href="' . esc_url( 'http://www.linkedin.com/shareArticle?mini=true&amp;url= ' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Linkedin', 'reendex' ) . '" target="_blank"><i class="fa fa-linkedin"></i></a>';
	}
	if ( ( 'show' === $reendex_share_pinterest ) && ( 'disable' === get_theme_mod( 'reendex_share_pinterest' ) ) || ! ( 'hide' === $reendex_share_pinterest ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_pinterest' ) ) ) {
		echo '<a class="pinterest-share-btn btn share-button" href="' . esc_url( 'https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Pinterest', 'reendex' ) . '" target="_blank"><i class="fa fa-pinterest"></i></a>';
	}
	if ( ( 'show' === $reendex_share_tumblr ) && ( 'disable' === get_theme_mod( 'reendex_share_tumblr' ) ) || ! ( 'hide' === $reendex_share_tumblr ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_tumblr' ) ) ) {
		echo '<a class="tumblr-share-btn btn share-button" href="' . esc_url( 'http://www.tumblr.com/share/link?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Tumblr', 'reendex' ) . '" target="_blank"><i class="fa fa-tumblr"></i></a>';
	}
	if ( ( 'show' === $reendex_share_reddit ) && ( 'disable' === get_theme_mod( 'reendex_share_reddit' ) ) || ! ( 'hide' === $reendex_share_reddit ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_reddit' ) ) ) {
		echo '<a class="reddit-share-btn btn share-button" href="' . esc_url( 'http://www.reddit.com/submit?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Reddit', 'reendex' ) . '" target="_blank"><i class="fa fa-reddit"></i></a>';
	}
	if ( ( 'show' === $reendex_share_stumbleupon ) && ( 'disable' === get_theme_mod( 'reendex_share_stumbleupon' ) ) || ! ( 'hide' === $reendex_share_stumbleupon ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_stumbleupon' ) ) ) {
		echo '<a class="stumbleupon-share-btn btn share-button" href="' . esc_url( 'http://www.stumbleupon.com/submit?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Stumbleupon', 'reendex' ) . '" target="_blank"><i class="fa fa-stumbleupon"></i></a>';
	}
	if ( ( 'show' === $reendex_share_digg ) && ( 'disable' === get_theme_mod( 'reendex_share_digg' ) ) || ! ( 'hide' === $reendex_share_digg ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_digg' ) ) ) {
		echo '<a class="digg-share-btn btn share-button" href="' . esc_url( 'http://digg.com/submit?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Digg', 'reendex' ) . '" target="_blank"><i class="fa fa-digg"></i></a>';
	}
	if ( ( 'show' === $reendex_share_vk ) && ( 'disable' === get_theme_mod( 'reendex_share_vk' ) ) || ! ( 'hide' === $reendex_share_vk ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_vk' ) ) ) {
		echo '<a class="vk-share-btn btn share-button" href="' . esc_url( 'http://vk.com/share.php?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Vk', 'reendex' ) . '" target="_blank"><i class="fa fa-vk"></i></a>';
	}
	if ( ( 'show' === $reendex_share_pocket ) && ( 'disable' === get_theme_mod( 'reendex_share_pocket' ) ) || ! ( 'hide' === $reendex_share_pocket ) && ! ( 'enable' !== get_theme_mod( 'reendex_share_pocket' ) ) ) {
		echo '<a class="pocket-share-btn btn share-button" href="' . esc_url( 'https://getpocket.com/save?url=' . get_the_permalink() . '' ) . '" title="' . esc_html__( 'Pocket', 'reendex' ) . '" target="_blank"><i class="fa fa-get-pocket"></i></a>';
	}
	?>
</div><!-- /.share-buttons -->
