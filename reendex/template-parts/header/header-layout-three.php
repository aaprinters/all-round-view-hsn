<?php
/**
 * Header style 3
 *
 * @package Reendex
 */

if ( ! function_exists( 'reendex_top_menu' ) ) :
	/**
	 * Top Menu codes
	 */
	function reendex_top_menu() {
		?>
		<div class="top-menu"> 
			<div class="container">        
				<?php get_template_part( 'template-parts/content', 'topbar' );?>
			</div>                     
		</div><!-- /.top-menu -->                 
		<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_top_menu', 20 );

if ( ! function_exists( 'reendex_site_branding' ) ) :
	/**
	 * Site branding Logo
	 */
	function reendex_site_branding() {
	?>
		<div class="container header-style-three">
			<div class="logo-ad-wrapper clearfix">
				<?php
				get_template_part( 'template-parts/header/header', 'site-branding-logo' );
				get_template_part( 'template-parts/header/header', 'site-ad-banner' );
				?>
			</div><!-- /.logo-ad-wrapper clearfix -->
		</div><!-- /.container header-style-three -->
		<div class="container-fluid">
			<?php do_action( 'reendex_menu' ); ?>
		</div>
	<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_branding', 40 );

if ( ! function_exists( 'reendex_site_navigation' ) ) :
	/**
	 * Site navigation codes
	 */
	function reendex_site_navigation() {
	?>
	<div class="container-fluid header-style-three">	    
		<?php get_template_part( 'template-parts/header/header', 'main-menu' ); ?>
	</div>
	<?php
	}
endif;
add_action( 'reendex_header_action', 'reendex_site_navigation', 30 );
