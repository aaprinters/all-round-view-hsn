<?php
/**
 * Mobile Menu.
 *
 * @package Reendex
 */

/**
 * Mobile Menu code.
 */
function reendex_menu() {
	$reendex_rm_position = get_theme_mod( 'reendex_rm_position', 'left' );
	?>
	<div id="mobile-nav-outer-wrapper">
		<div id="mobile-nav-wrapper" class="navbar navbar-default">
			<div class="mobile-menu-logo">
				<?php if ( get_theme_mod( 'reendex_logo' ) ) : ?>
					<div class='site-logo'>
						<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'reendex_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
					</div>
				<?php else : ?>
					<div class="site-branding-text">
						<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
						<h4 class='site-description'><?php bloginfo( 'description' ); ?></h4>
					</div>
				<?php endif; ?>
			</div><!-- /.mobile-menu-logo -->
		<?php if ( has_nav_menu( 'offcanvasmenu' ) ) { ?>
			<div id="wprmenu_bar" class="wprmenu_bar navbar navbar-default">
				<div class="wprmenu_icon">
					<span class="wprmenu_ic_1"></span>
					<span class="wprmenu_ic_2"></span>
					<span class="wprmenu_ic_3"></span>
				</div>
			</div><!-- /#wprmenu_bar -->
		</div><!-- /#mobile-nav-wrapper -->
		<div id="wprmenu_menu" class="wprmenu_levels <?php echo esc_attr( $reendex_rm_position ); ?> wprmenu_custom_icons sidebar-closed">
			<div class="mobile-menu-top-wrapper">
			<!-- Begin .mobile-menu-logo -->
			<div class="mobile-menu-logo">
				<?php if ( get_theme_mod( 'reendex_logo' ) ) : ?>
					<div class='site-logo'>
						<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'reendex_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
					</div>
				<?php else : ?>
					<div class="site-branding-text">
						<h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
						<h4 class='site-description'><?php bloginfo( 'description' ); ?></h4>
					</div>
				<?php endif; ?>
			</div><!-- /.mobile-menu-logo -->
			<div class="menu-close">
			<a class="js-toggleSidebar sidebar-close">
				<span class="pe-7s-close"></span>
			</a>
			</div>
			</div>
			<?php if ( get_theme_mod( 'reendex_rm_search_box', 'below_menu' ) == 'above_menu' ) { ?> 
				<div class="wpr_search">
					<?php get_search_form(); ?>
				</div>
			<?php } ?>
			<ul id="wprmenu_menu_ul">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'offcanvasmenu',
							'container' => false,
							'items_wrap' => '%3$s',
						)
					);
				?>
			</ul>
			<?php if ( get_theme_mod( 'reendex_rm_search_box', 'below_menu' ) == 'below_menu' ) { ?> 
			<div class="wpr_search">
				<?php get_search_form(); ?>
			</div>
			<?php } ?>
					<?php
					$options = reendex_get_theme_options();
					$show_social = get_theme_mod( 'reendex_rm_socialmedia_show', 'enable' );
					if ( 'enable' === $show_social ) : ?>
			<div class="mobile-social-icons">
				<ul>
					<?php
					$facebook = $options['reendex_facebook_link'];
					$twitter = $options['reendex_twitter_link'];
					$pinterest = $options['reendex_pinterest_link'];
					$g = $options['reendex_googleplus_link'];
					$vimeo = $options['reendex_vimeo_link'];
					$youtube = $options['reendex_youtube_link'];
					$linkedin = $options['reendex_linkedin_link'];
					$rss = $options['reendex_rss_link'];
					if ( strlen( $facebook ) > 0 ) {
						echo '<li class="facebook"><a class="facebook" href="' . esc_url( $facebook ) . '"><i class="fa fa-facebook"></i></a></li>';
					}
					if ( strlen( $twitter ) > 0 ) {
						echo '<li class="twitter"><a class="twitter" href="' . esc_url( $twitter ) . '"><i class="fa fa-twitter"></i></a></li>';
					}
					if ( strlen( $pinterest ) > 0 ) {
						echo '<li class="pinterest"><a class="pinterest" href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
					}
					if ( strlen( $g ) > 0 ) {
						echo '<li class="gplus"><a class="google-plus" href="' . esc_url( $g ) . '"><i class="fa fa-google-plus"></i></a></li>';
					}
					if ( strlen( $vimeo ) > 0 ) {
						echo '<li class="vimeo"><a class="vimeo" href="' . esc_url( $vimeo ) . '"><i class="fa fa-vimeo-square"></i></a></li>';
					}
					if ( strlen( $youtube ) > 0 ) {
						echo '<li class="youtube"><a class="youtube" href="' . esc_url( $youtube ) . '"><i class="fa fa-youtube"></i></a></li>';
					}
					if ( strlen( $linkedin ) > 0 ) {
						echo '<li class="linkedin"><a class="linkedin" href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a></li>';
					}
					if ( strlen( $rss ) > 0 ) {
						echo '<li class="rss"><a class="rss" href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a></li>';
					}
					?>
				</ul>
			</div><!-- /.mobile-social-icons -->
			<?php endif;?>
		</div><!-- /#wprmenu_menu -->
		<?php } // End if(). ?>
	</div><!-- /#mobile-nav-outer-wrapper -->
	<?php
}
