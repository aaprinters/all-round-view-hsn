<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post entry-content">
		<div class="content-inner">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links pagination"><span class="page-links-title">' . esc_html__( 'Pages:', 'reendex' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'reendex' ) . ' </span>%',
			) );
		?>
		</div><!-- /.content-inner -->
	</div><!-- /.post entry-content -->
</article><!-- #post-## -->
