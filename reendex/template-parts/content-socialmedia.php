<?php
/**
 * The template for displaying social media icons
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
?>

<div class="footer-social-icons">
	<ul>
		<?php
			$facebook = $options['reendex_facebook_link'];
			$twitter = $options['reendex_twitter_link'];
			$g = $options['reendex_googleplus_link'];
			$linkedin = $options['reendex_linkedin_link'];
			$pinterest = $options['reendex_pinterest_link'];
			$tumblr = $options['reendex_tumblr_link'];
			$reddit = $options['reendex_reddit_link'];
			$stumbleupon = $options['reendex_stumbleupon_link'];
			$digg = $options['reendex_digg_link'];
			$vimeo = $options['reendex_vimeo_link'];
			$youtube = $options['reendex_youtube_link'];
			$rss = $options['reendex_rss_link'];
			$instagram = $options['reendex_instagram_link'];
		if ( strlen( $facebook ) > 0 ) {
			echo '<li class="facebook"><a class="facebook" href="' . esc_url( $facebook ) . '"><i class="fa fa-facebook"></i></a></li>';
		}
		if ( strlen( $twitter ) > 0 ) {
			echo '<li class="twitter"><a class="twitter" href="' . esc_url( $twitter ) . '"><i class="fa fa-twitter"></i></a></li>';
		}
		if ( strlen( $g ) > 0 ) {
			echo '<li class="gplus"><a class="google-plus" href="' . esc_url( $g ) . '"><i class="fa fa-google-plus"></i></a></li>';
		}
		if ( strlen( $linkedin ) > 0 ) {
			echo '<li class="linkedin"><a class="linkedin" href="' . esc_url( $linkedin ) . '"><i class="fa fa-linkedin"></i></a></li>';
		}
		if ( strlen( $pinterest ) > 0 ) {
			echo '<li class="pinterest"><a class="pinterest" href="' . esc_url( $pinterest ) . '"><i class="fa fa-pinterest-p"></i></a></li>';
		}
		if ( strlen( $tumblr ) > 0 ) {
			echo '<li class="tumblr"><a class="tumblr" href="' . esc_url( $tumblr ) . '"><i class="fa fa-tumblr"></i></a></li>';
		}
		if ( strlen( $reddit ) > 0 ) {
			echo '<li class="reddit"><a class="reddit" href="' . esc_url( $reddit ) . '"><i class="fa fa-reddit"></i></a></li>';
		}
		if ( strlen( $stumbleupon ) > 0 ) {
			echo '<li class="stumbleupon"><a class="stumbleupon" href="' . esc_url( $stumbleupon ) . '"><i class="fa fa-stumbleupon"></i></a></li>';
		}
		if ( strlen( $digg ) > 0 ) {
			echo '<li class="digg"><a class="digg" href="' . esc_url( $digg ) . '"><i class="fa fa-digg"></i></a></li>';
		}
		if ( strlen( $vimeo ) > 0 ) {
			echo '<li class="vimeo"><a class="vimeo" href="' . esc_url( $vimeo ) . '"><i class="fa fa-vimeo-square"></i></a></li>';
		}
		if ( strlen( $youtube ) > 0 ) {
			echo '<li class="youtube"><a class="youtube" href="' . esc_url( $youtube ) . '"><i class="fa fa-youtube"></i></a></li>';
		}
		if ( strlen( $rss ) > 0 ) {
			echo '<li class="rss"><a class="rss" href="' . esc_url( $rss ) . '"><i class="fa fa-rss"></i></a></li>';
		}
		if ( strlen( $instagram ) > 0 ) {
			echo '<li class="instagram"><a class="instagram" href="' . esc_url( $instagram ) . '"><i class="fa fa-instagram"></i></a></li>';
		}
		?>
	</ul>
</div><!-- /.footer-social-icons -->
