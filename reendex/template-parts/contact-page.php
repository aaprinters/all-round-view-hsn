<?php
/** 
 * Template Name: Contact Page One.
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
$banner_contact_image_link = reendex_get_default_contact_banner();
$banner_title = get_option( 'reendex_contact_banner_title' );
$banner_title = empty( $banner_title )? esc_html__( 'Contact Us', 'reendex' ): $banner_title;
$banner_subtitle = get_option( 'reendex_contact_banner_subtitle' );
$banner_subtitle = empty( $banner_subtitle )? esc_html__( 'Look for more news from our own sources', 'reendex' ): $banner_subtitle;
$banner_subtitle_link = $options['reendex_contact_banner_subtitle_link'];
$show_contact_map_title = get_theme_mod( 'reendex_contact_map_title_show', 'enable' );
$contact_map_title = $options['reendex_contact_map_title'];
$show_map = get_theme_mod( 'reendex_gmap_show', 'enable' );
$contact_page_ab_title = $options['reendex_contact_page_ab_title'];
$reendex_contact_aboutus_text = $options['reendex_contact_aboutus_text'];
$contact_page_info_title = $options['reendex_contact_page_info_title'];
$reendex_our_address = $options['reendex_our_address'];
$reendex_contact = $options['reendex_contact'];
$reendex_email = $options['reendex_email'];
$google_address = $options['reendex_google_address'];
$reendex_gmap_zoom = $options['reendex_gmap_zoom'];
?>
	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
	<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_contact_banner_show', 'disable' ) ) ) : ?>
		<div class="contact-page-header">
			<div class="archive-page-header">
				<div class="container-fluid">
					<div>
						<div class="archive-nav-inline">
							<div id="parallax-section3">
								<div class="image1 img-overlay3" style="background-image: url('<?php echo esc_url( $banner_contact_image_link ); ?>'); ">
									<?php if ( ! ( 'enable' !== get_theme_mod( 'reendex_contact_banner_title_show', 'enable' ) ) ) : ?>
										<div class="container archive-menu">
											<h1 class="page-title"><span><?php echo esc_html( $banner_title ); ?></span></h1>
											<h4 class="page-subtitle"><span><a href="<?php echo esc_url( $banner_subtitle_link ); ?>"><?php echo esc_html( $banner_subtitle ); ?></a></span></h4>
										</div>
									<?php endif; ?>								
								</div><!-- /.image1 img-overlay3 -->
							</div><!-- /.parallax-section3 -->
						</div><!-- /.archive-nav-inline -->
					</div>
				</div><!-- /.container-fluid -->
			</div><!-- /.archive-page-header -->
		</div><!-- /.contact-page-header -->
	<?php endif; ?>	
<section class="module-top">
	<div class="contact-page container">
		<div class="contact-us">
			<div class="row no-gutter">
				<div class="<?php if ( 'enable' === $show_map ) {
					echo 'col-xs-12 col-sm-5 col-md-5';
} else {
	echo 'col-xs-12 col-sm-12 col-md-12';
} ?>">
					<div class="form-group">
						<div class="title-left title-style04 underline04">
							<h3><?php echo esc_attr( $contact_page_ab_title );?></h3>
						</div>
						<p><?php echo wp_kses_post( $reendex_contact_aboutus_text ) ?></p>
						<div class="title-left title-style04 underline04">
							<h3><?php echo esc_attr( $contact_page_info_title );?></h3>
						</div>
						<ul>
							<li>
								<i class="fa fa-map-marker"></i>
								<span><?php echo esc_html_e( 'Address:','reendex' ); ?></span> <?php echo esc_html( $reendex_our_address ); ?>
							</li>
							<li>
								<i class="fa fa-phone"></i>
								<span><?php echo esc_html_e( 'Phone:','reendex' ); ?></span> <?php echo esc_html( $reendex_contact ); ?>
							</li>
							<li>
								<i class="fa fa-envelope-o"></i>
								<span><?php echo esc_html_e( 'E-mail:','reendex' ); ?></span> <?php echo esc_html( $reendex_email ); ?>
							</li>
						</ul>
					</div><!-- /.form-group -->
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7">
					<?php if ( 'enable' === $show_map ) {
						if ( 'disable' !== $show_contact_map_title ) : ?>
							<div class="title-left title-style04 underline04">
								<h3><?php echo esc_attr( $contact_map_title );?></h3>
							</div> 
						<?php endif; ?>
						<div class="map-container clearfix">
							<div id="map_canvas">
								<?php
								$a  = $google_address;
								$z = intval( $reendex_gmap_zoom );
								$i = 'near';
								$iframe_tag = 'iframe';
								echo '<' . esc_attr( $iframe_tag ) . ' src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=' . esc_attr( $a ) . '&amp;ie=UTF8&amp;hq=&amp;hnear=' . esc_attr( $a ) . '&amp;z=' . esc_attr( $z ) . '&amp;output=embed&amp;iwloc=' . esc_attr( $i ) . '"></' . esc_attr( $iframe_tag ) . '>';
								?>
							</div><!-- /.map_canvas --> 
						</div><!-- /.map-container clearfix --> 
					<?php }
					?>
				</div><!-- /.col -->
			</div><!-- /.row no-gutter -->
		</div><!-- /.contact-us -->
		<div class="row no-gutter">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="title-left title-style04 underline04">
					<?php
						$contact_form_title = $options['reendex_contact_form_title'];
					?>
					<h3><?php echo esc_attr( $contact_form_title );?></h3>
				</div>
				<div class="contact-formarea">
					<?php
					$reendex_contact_form = get_option( 'reendex_contact_form' );
					echo do_shortcode( $reendex_contact_form );?>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row no-gutter -->
	</div><!-- /.contact-page container -->
</section><!-- /.module -->

<?php get_footer(); ?>
