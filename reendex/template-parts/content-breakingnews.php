<?php
/**
 * Template part for displaying Breaking News for post.
 *
 * @package Reendex
 */

$options = reendex_get_theme_options();
?>
<div class="newsticker-area-single">
	<div class="container">
		<div class="outer-single"> 
			<div class="breaking-ribbon"> 
				<h4><?php esc_html_e( 'Breaking News','reendex' );?></h4> 
			</div>                             
			<div class="newsticker">
				<ul>
					<?php
					$reendex_single_ticker_content = $options['reendex_single_ticker_content'];
					$recent_posts = wp_get_recent_posts();
					foreach ( $recent_posts as $single_post ) {
					?>
						<li>
							<h4>   
								<a href="<?php echo esc_url( get_permalink( $single_post['ID'] ) ); ?>" title="<?php echo esc_attr( $single_post['post_title'] ); ?>">
									<?php echo esc_html( wp_trim_words( $single_post['post_content'], $reendex_single_ticker_content ) ); ?>
								</a>
							</h4>
						</li>
					<?php
					}
					?>
				</ul>
				<div class="navi"> 
					<button class="up">
						<i class="fa fa-caret-left"></i>
					</button>                                     
					<button class="down">
						<i class="fa fa-caret-right"></i>
					</button>                                     
				</div>                                 
			</div><!-- /.newsticker -->                           
		</div><!-- /.outer-single -->
	</div><!-- /.container -->
</div><!-- /.newsticker-area-single -->
