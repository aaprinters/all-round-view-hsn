<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Reendex
 */

get_header();
$options = reendex_get_theme_options();
$show_related_video_title = get_theme_mod( 'reendex_related_video_title_show', 'enable' );
$related_video_title = $options['reendex_related_video_title'];
$related_video_subtitle = $options['reendex_related_video_subtitle'];
$related_video_title_bg_color = get_theme_mod( 'reendex_related_video_title_bg_color' );
$reendex_breaking_news = get_post_meta( get_the_ID(), 'reendex_breaking_news', true );
?>
	<?php
	if ( ! current_user_can( 'edit_themes' ) || ! is_user_logged_in() ) {
		$show_comingsoon = get_theme_mod( 'reendex_comingsoon_show', 'disable' );
		if ( 'disable' !== $show_comingsoon ) {
					get_template_part( 'coming', 'soon' );
					exit();
		}
	}
	?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">		    
				<?php
					// Meta box data.
					$reendex_id = get_the_ID();
					$reendex_page_breadcrumbs = get_post_meta( $reendex_id,'reendex_page_breadcrumbs',true );
				if ( 'show' == $reendex_page_breadcrumbs ) {
					reendex_custom_breadcrumbs();
				}
				?>
			</div><!-- /.container -->
<?php
if ( ( 'hide' != $reendex_breaking_news ) ) : ?>
	<div class="container breaking-ticker">
	<?php get_template_part( 'template-parts/content','breakingnews' ); ?>
	</div><!-- /.container breaking-ticker -->
<?php endif;
?>
			<div class="row no-gutter"> 
				<div class="content-wrap">
					<?php
					$vid_page_layout = $options['reendex_video-options'];
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							if ( 'normal' != $vid_page_layout ) {
								get_template_part( 'template-parts/content', 'projectvideofull' ); } else {
								get_template_part( 'template-parts/content', 'projectvideo' );
								}
							endwhile;
						endif;
					?>
				</div><!-- /.content-wrap -->

					<?php
					$show_related_video = get_theme_mod( 'reendex_related_video_show', 'enable' );
					if ( 'disable' !== $show_related_video ) : ?>
					<div class="container mt-40">
						<div class="media col-sm-12 col-md-12 col-lg-12">
							<?php get_template_part( 'inc/reendex-related-videos' ); ?>
						</div><!-- /.media -->
					</div><!-- /.container -->
					<?php endif;?>
		
				<div class="container mb-50 mt-40">
					<?php
					$show_gallery = get_theme_mod( 'reendex_gallery_show', 'enable' );
					if ( 'disable' !== $show_gallery ) :?> 
						<?php
							$video_page_gallery_title = $options['reendex_gallery_title'];
							$video_page_gallery_title_bg_color = get_theme_mod( 'reendex_video_page_gallery_title_bg_color' );
						?>
						<h3 class="carousel-title mb-5" style="<?php if ( '' != $video_page_gallery_title_bg_color ) { echo 'background-color:' . esc_attr( $video_page_gallery_title_bg_color ) . ';'; }?>"><?php echo esc_attr( $video_page_gallery_title );?></h3>
						<div id="big-gallery-slider-3" class="owl-carousel">
						<?php
						$args = array(
							'post_type'            => 'our-video',
							'post_status'          => 'publish',
							'ignore_sticky_posts'  => 1,
						);
						$video_categories = $options['reendex_video_categories'];
						$categories = str_replace( ' ', '', $video_categories );
						if ( strlen( $categories ) > 0 ) {
							$ar_categories = explode( ',', $categories );
							if ( is_array( $ar_categories ) && count( $ar_categories ) > 0 ) {
								$field_name = is_numeric( $ar_categories[0] )?'term_id':'slug';
								$args['tax_query'] = array(
									array(
										'taxonomy'         => 'thcat_taxonomy',
										'terms'            => $ar_categories,
										'field'            => $field_name,
										'include_children' => false,
									),
								);
							}
						}
						$query = new WP_Query( $args );
						while ( $query->have_posts() ) : $query->the_post();
					?>
						<div class="big-gallery">
							<?php
								the_post_thumbnail( 'reendex_video_gallery_thumb',
									array(
										'class' => 'img-not-lazy',
									)
								);
							?>
							<a href="<?php the_permalink();?>"><span class="play-icon"></span></a> 
						</div><!-- /.big-gallery -->
						<?php endwhile;?>
						</div>
					<?php endif;?>
					<?php if ( ! function_exists( 'reendex_video_ad' ) ) :
						$video_ad_image 	= get_theme_mod( 'video_ad_image' );
						$video_ad_url		= get_theme_mod( 'video_ad_url' );
						$attachment_url     = attachment_url_to_postid( $video_ad_image );
						$image_alt          = get_post_meta( $attachment_url, '_wp_attachment_image_alt', true );
					?>
					<?php if ( $video_ad_image ) : ?>
						<div class="ad-place video_page">
							<a target="_blank" href="<?php echo esc_url( $video_ad_url ); ?>"><img src="<?php echo esc_url( $video_ad_image ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>"/></a>
						</div>
					<?php endif; ?>
					<?php endif; ?>
				</div><!-- /.container -->
			</div><!-- /.row no-gutter -->
		</main><!-- /#main -->
	</div><!-- /#primary -->

<?php
get_footer();
