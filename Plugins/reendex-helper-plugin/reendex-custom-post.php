<?php
/**
 * Custom Post Type
 *
 * @package Reendex
 */

 /**
  *  Register Custom Post Type
  */
function reendex_custom_post() {

	$labels = array(
		'name'                  => _x( 'Video', 'Post Type General Name', 'reendex' ),
		'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'reendex' ),
		'menu_name'             => esc_html__( 'Video', 'reendex' ),
		'name_admin_bar'        => esc_html__( 'Video', 'reendex' ),
		'archives'              => esc_html__( 'Item Archives', 'reendex' ),
		'parent_item_colon'     => esc_html__( 'Parent Item:', 'reendex' ),
		'all_items'             => esc_html__( 'All Video Item', 'reendex' ),
		'add_new_item'          => esc_html__( 'Add New Video', 'reendex' ),
		'add_new'               => esc_html__( 'Add New Video', 'reendex' ),
		'new_item'              => esc_html__( 'Add New Video', 'reendex' ),
		'edit_item'             => esc_html__( 'Edit Video', 'reendex' ),
		'update_item'           => esc_html__( 'Update Video', 'reendex' ),
		'view_item'             => esc_html__( 'View Video', 'reendex' ),
		'search_items'          => esc_html__( 'Search Video', 'reendex' ),
		'not_found'             => esc_html__( 'Not found', 'reendex' ),
		'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'reendex' ),
		'featured_image'        => esc_html__( 'Video Image', 'reendex' ),
		'set_featured_image'    => esc_html__( 'Set Video Image', 'reendex' ),
		'remove_featured_image' => esc_html__( 'Remove Video Image', 'reendex' ),
		'use_featured_image'    => esc_html__( 'Use as Video Image', 'reendex' ),
		'insert_into_item'      => esc_html__( 'Insert into item', 'reendex' ),
		'uploaded_to_this_item' => esc_html__( 'Uploaded to this item', 'reendex' ),
		'items_list'            => esc_html__( 'Items list', 'reendex' ),
		'items_list_navigation' => esc_html__( 'Items list navigation', 'reendex' ),
		'filter_items_list'     => esc_html__( 'Filter items list', 'reendex' ),
	);
	$args = array(
		'label'                 => esc_html__( 'Video', 'reendex' ),
		'description'           => esc_html__( 'Video', 'reendex' ),
		'labels'                => $labels,
		'supports'              => array( 'editor','thumbnail','title' ),
		'taxonomies'            => array( 'video_cat','video_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon' => 'dashicons-video-alt',
	);
	register_post_type( 'our-video', $args );

	// Tv Schedule.
	$labels = array(
		'name'                  => _x( 'TV Schedules', 'Post Type General Name', 'reendex' ),
		'singular_name'         => _x( 'TV Schedule', 'Post Type Singular Name', 'reendex' ),
		'menu_name'             => esc_html__( 'TV Schedule', 'reendex' ),
		'name_admin_bar'        => esc_html__( 'TV Schedule', 'reendex' ),
		'archives'              => esc_html__( 'TV Schedule Archives', 'reendex' ),
		'attributes'            => esc_html__( 'TV Schedule Attributes', 'reendex' ),
		'parent_item_colon'     => esc_html__( 'Parent Item:', 'reendex' ),
		'all_items'             => esc_html__( 'All TV Schedule', 'reendex' ),
		'add_new_item'          => esc_html__( 'Add New TV Schedule', 'reendex' ),
		'add_new'               => esc_html__( 'Add New TV Schedule', 'reendex' ),
		'new_item'              => esc_html__( 'New TV Schedule', 'reendex' ),
		'edit_item'             => esc_html__( 'Edit TV Schedule', 'reendex' ),
		'update_item'           => esc_html__( 'Update TV Schedule', 'reendex' ),
		'view_item'             => esc_html__( 'View TV Schedule', 'reendex' ),
		'view_items'            => esc_html__( 'View TV Schedules', 'reendex' ),
		'search_items'          => esc_html__( 'Search TV Schedule', 'reendex' ),
		'not_found'             => esc_html__( 'Not found', 'reendex' ),
		'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'reendex' ),
		'featured_image'        => esc_html__( 'TV Schedule Image', 'reendex' ),
		'set_featured_image'    => esc_html__( 'Set TV Schedule image', 'reendex' ),
		'remove_featured_image' => esc_html__( 'Remove TV Schedule image', 'reendex' ),
		'use_featured_image'    => esc_html__( 'Use as TV Schedule image', 'reendex' ),
		'insert_into_item'      => esc_html__( 'Insert into item', 'reendex' ),
		'uploaded_to_this_item' => esc_html__( 'Uploaded to this item', 'reendex' ),
		'items_list'            => esc_html__( 'TV Schedule list', 'reendex' ),
		'items_list_navigation' => esc_html__( 'TV Schedule list navigation', 'reendex' ),
		'filter_items_list'     => esc_html__( 'Filter items list', 'reendex' ),
	);
	$args = array(
		'label'                 => esc_html__( 'TV Schedule', 'reendex' ),
		'description'           => esc_html__( 'Post Type Description', 'reendex' ),
		'labels'                => $labels,
		'supports'              => array( 'editor','thumbnail','title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon' => 'dashicons-admin-users',
	);
	register_post_type( 'tv_schedule', $args );

	// Our Team.
	$labels = array(
		'name'                  => _x( 'Team Member', 'Post Type General Name', 'reendex' ),
		'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', 'reendex' ),
		'menu_name'             => esc_html__( 'Our Team', 'reendex' ),
		'name_admin_bar'        => esc_html__( 'Team Member', 'reendex' ),
		'archives'              => esc_html__( 'Item Archives', 'reendex' ),
		'attributes'            => esc_html__( 'Item Attributes', 'reendex' ),
		'parent_item_colon'     => esc_html__( 'Parent Item:', 'reendex' ),
		'all_items'             => esc_html__( 'Team Members', 'reendex' ),
		'add_new_item'          => esc_html__( 'Add New Member', 'reendex' ),
		'add_new'               => esc_html__( 'Add New Member', 'reendex' ),
		'new_item'              => esc_html__( 'New Member', 'reendex' ),
		'edit_item'             => esc_html__( 'Edit Member', 'reendex' ),
		'update_item'           => esc_html__( 'Update Member', 'reendex' ),
		'view_item'             => esc_html__( 'View Member', 'reendex' ),
		'view_items'            => esc_html__( 'View Members', 'reendex' ),
		'search_items'          => esc_html__( 'Search Member', 'reendex' ),
		'not_found'             => esc_html__( 'Not found', 'reendex' ),
		'not_found_in_trash'    => esc_html__( 'Not found in Trash', 'reendex' ),
		'featured_image'        => esc_html__( 'Team Member Image', 'reendex' ),
		'set_featured_image'    => esc_html__( 'Team Member image', 'reendex' ),
		'remove_featured_image' => esc_html__( 'Remove Team Member image', 'reendex' ),
		'use_featured_image'    => esc_html__( 'Use as Team Member image', 'reendex' ),
		'insert_into_item'      => esc_html__( 'Insert into Member', 'reendex' ),
		'uploaded_to_this_item' => esc_html__( 'Uploaded to this Member', 'reendex' ),
		'items_list'            => esc_html__( 'Member list', 'reendex' ),
		'items_list_navigation' => esc_html__( 'Members list navigation', 'reendex' ),
		'filter_items_list'     => esc_html__( 'Filter Member list', 'reendex' ),
	);
	$args = array(
		'label'                 => esc_html__( 'Team Member', 'reendex' ),
		'description'           => esc_html__( 'Post Type Description', 'reendex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon' => 'dashicons-businessman',
	);
	register_post_type( 'our_team', $args );

}
add_action( 'init', 'reendex_custom_post' );
 /**
  * Register Custom Taxonomy
  */
function reendex_custom_taxonomy() {

	// Taxonomies Category for video.
	$labels = array(
		'name'                       => _x( 'Video Categories', 'Taxonomy General Name', 'reendex' ),
		'singular_name'              => _x( 'Video Categories', 'Taxonomy Singular Name', 'reendex' ),
		'menu_name'                  => esc_html__( 'Video Categories', 'reendex' ),
		'all_items'                  => esc_html__( 'All Items', 'reendex' ),
		'parent_item'                => esc_html__( 'Parent Item', 'reendex' ),
		'parent_item_colon'          => esc_html__( 'Parent Item:', 'reendex' ),
		'new_item_name'              => esc_html__( 'New Video Categories', 'reendex' ),
		'add_new_item'               => esc_html__( 'Add New Categories', 'reendex' ),
		'edit_item'                  => esc_html__( 'Edit Categories', 'reendex' ),
		'update_item'                => esc_html__( 'Update Categories', 'reendex' ),
		'view_item'                  => esc_html__( 'View Categories', 'reendex' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'reendex' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove Categories', 'reendex' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'reendex' ),
		'popular_items'              => esc_html__( 'Popular Items', 'reendex' ),
		'search_items'               => esc_html__( 'Search Categories', 'reendex' ),
		'not_found'                  => esc_html__( 'Not Found', 'reendex' ),
		'no_terms'                   => esc_html__( 'No Categories', 'reendex' ),
		'items_list'                 => esc_html__( 'Categories list', 'reendex' ),
		'items_list_navigation'      => esc_html__( 'Items list navigation', 'reendex' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'thcat_taxonomy', array( 'our-video' ), $args );

	// Taxonomies Tags for Video.
	$labels = array(
		'name'                       => _x( 'Tags', 'Taxonomy General Name', 'reendex' ),
		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'reendex' ),
		'menu_name'                  => esc_html__( 'Tags', 'reendex' ),
		'all_items'                  => esc_html__( 'All Tags', 'reendex' ),
		'parent_item'                => esc_html__( 'Parent Tags', 'reendex' ),
		'parent_item_colon'          => esc_html__( 'Parent Tags:', 'reendex' ),
		'new_item_name'              => esc_html__( 'New Tags Name', 'reendex' ),
		'add_new_item'               => esc_html__( 'Add New Tags', 'reendex' ),
		'edit_item'                  => esc_html__( 'Edit Tags', 'reendex' ),
		'update_item'                => esc_html__( 'Update Tags', 'reendex' ),
		'view_item'                  => esc_html__( 'View Tags', 'reendex' ),
		'separate_items_with_commas' => esc_html__( 'Separate Tags with commas', 'reendex' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove Tags', 'reendex' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'reendex' ),
		'popular_items'              => esc_html__( 'Popular Tags', 'reendex' ),
		'search_items'               => esc_html__( 'Search Tags', 'reendex' ),
		'not_found'                  => esc_html__( 'Not Found', 'reendex' ),
		'no_terms'                   => esc_html__( 'No Tags', 'reendex' ),
		'items_list'                 => esc_html__( 'Tags list', 'reendex' ),
		'items_list_navigation'      => esc_html__( 'Tags list navigation', 'reendex' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'thtag_taxonomy', array( 'our-video' ), $args );

	// Taxonomies Category for tv schedule.
	$labels = array(
		'name'                       => _x( 'TV Schedule Categories', 'Taxonomy General Name', 'reendex' ),
		'singular_name'              => _x( 'TV Schedule Category', 'Taxonomy Singular Name', 'reendex' ),
		'menu_name'                  => esc_html__( 'TV Schedule Category', 'reendex' ),
		'all_items'                  => esc_html__( 'All Category', 'reendex' ),
		'parent_item'                => esc_html__( 'Parent Item', 'reendex' ),
		'parent_item_colon'          => esc_html__( 'Parent Item:', 'reendex' ),
		'new_item_name'              => esc_html__( 'New Category Name', 'reendex' ),
		'add_new_item'               => esc_html__( 'Add New Category', 'reendex' ),
		'edit_item'                  => esc_html__( 'Edit Category', 'reendex' ),
		'update_item'                => esc_html__( 'Update Category', 'reendex' ),
		'view_item'                  => esc_html__( 'View Category', 'reendex' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'reendex' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove items', 'reendex' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'reendex' ),
		'popular_items'              => esc_html__( 'Popular Items', 'reendex' ),
		'search_items'               => esc_html__( 'Search Items', 'reendex' ),
		'not_found'                  => esc_html__( 'Not Found', 'reendex' ),
		'no_terms'                   => esc_html__( 'No items', 'reendex' ),
		'items_list'                 => esc_html__( 'Items list', 'reendex' ),
		'items_list_navigation'      => esc_html__( 'Items list navigation', 'reendex' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tv_category', array( 'tv_schedule' ), $args );

}
add_action( 'init', 'reendex_custom_taxonomy', 0 );
