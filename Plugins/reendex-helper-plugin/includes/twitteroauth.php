<?php
/**
 * Abraham Williams (abraham@abrah.am) http://abrah.am
 *
 * The first PHP Library to support OAuth for Twitter's REST API.
 *
 * @package Reendex
 */

namespace Reendex;

/* Load OAuth lib. You can find it at http://oauth.net */
require_once( 'OAuth.php' );

/**
 * Twitter OAuth class
 */
class TwitterOAuth {

	/** Contains the last HTTP status code returned.
	 *
	 * @var string
	 */
	public $http_code;

	/** Contains the last API call.
	 *
	 * @var string
	 */
	public $url;

	/** Set up the API root URL.
	 *
	 * @var string
	 */
	public $host = 'https://api.twitter.com/1/';

	/** Set timeout default.
	 *
	 * @var string $timeout
	 */
	public $timeout = 30;

	/** Set connect timeout.
	 *
	 * @var int
	 */
	public $connecttimeout = 30;

	/** Verify SSL Cert.
	 *
	 * @var boolean
	 */
	public $ssl_verifypeer = false;

	/** Respons format.
	 *
	 * @var string
	 */
	public $format = 'json';

	/** Decode returned json data.
	 *
	 * @var boolean
	 */
	public $decode_json = true;

	/** Contains the last HTTP headers returned.
	 *
	 * @var array
	 */
	public $http_info;

	/** Set the useragnet.
	 *
	 * @var string
	 */
	public $useragent = 'TwitterOAuth v0.2.0-beta2';

	/**
	 * Set API URLS
	 */

	/**
	 * Access Token URL
	 */
	function access_token_url() {
			return 'https://api.twitter.com/oauth/access_token'; }
	/**
	 * Authenticate URL.
	 */
	function authenticate_url() {
			return 'https://api.twitter.com/oauth/authenticate'; }
	/**
	 * Authorize URL.
	 */
	function authorize_url() {
			return 'https://api.twitter.com/oauth/authorize'; }
	/**
	 * Request Token URL.
	 */
	function request_token_url() {
			return 'https://api.twitter.com/oauth/request_token'; }

	/**
	 * Debug helpers
	 */

	/**
	 * Returns the last HTTP status code
	 *
	 * @return integer
	 */
	function last_status_code() {
			return $this->http_status; }
	/**
	 * Returns the URL of the last API call
	 *
	 * @return string
	 */
	function last_api_call() {
			return $this->last_api_call; }

	/**
	 * Constructor
	 *
	 * @param string      $consumer_key      The Application Consumer Key.
	 * @param string      $consumer_secret   The Application Consumer Secret.
	 * @param string|null $oauth_token      The Client Token (optional).
	 * @param string|null $oauth_token_secret The Client Token Secret (optional).
	 */
	function __construct( $consumer_key, $consumer_secret, $oauth_token = null, $oauth_token_secret = null ) {
		$this->sha1_method = new OAuthSignatureMethod_HMAC_SHA1();
		$this->consumer = new OAuthConsumer( $consumer_key, $consumer_secret );
		if ( ! empty( $oauth_token ) && ! empty( $oauth_token_secret ) ) {
			$this->token = new OAuthConsumer( $oauth_token, $oauth_token_secret );
		} else {
			$this->token = null;
		}
	}

	/**
	 * Get a request token from Twitter's API
	 *
	 * @param string|null $oauth_callback Callback URL for Twitter to send a response to when the token is created.
	 * @return array Contains OAuth token and OAuth token secret information.
	 */
	function get_request_token( $oauth_callback = null ) {
		$parameters = array();
		if ( ! empty( $oauth_callback ) ) {
			$parameters['oauth_callback'] = $oauth_callback;
		}
		$request = $this->oAuthRequest( $this->requestTokenURL(), 'GET', $parameters );
		$token = OAuthUtil::parse_parameters( $request );
		$this->token = new OAuthConsumer( $token['oauth_token'], $token['oauth_token_secret'] );
		return $token;
	}

	/**
	 * Get the proper URL to use for authorization or authentication depending on whether a full
	 * sign in request is being issued via Twitter.
	 *
	 * @param string $token Contains the end user's OAuth token.
	 * @param bool   $sign_in_with_twitter Sign in request.
	 * @return string URL to redirect the end user to for authentication or authorization.
	 */
	function get_authorize_url( $token, $sign_in_with_twitter = true ) {
		if ( is_array( $token ) ) {
			$token = $token['oauth_token'];
		}
		if ( empty( $sign_in_with_twitter ) ) {
			return $this->authorizeURL() . "?oauth_token={$token}";
		} else {
			return $this->authenticateURL() . "?oauth_token={$token}";
		}
	}

	/**
	 * Use the OAuth verifier parameter returned by Twitter to the registered callback to
	 * obtain a new access token and token secret that can be used to sign future API calls
	 * by the application on behalf of the user.
	 *
	 * The token returned by getAccessToken is an array containing Twitter user information:
	 *
	 *  array( 'oauth_token' => 'the-access-token',
	 *         'oauth_token_secret' => 'the-access-secret',
	 *         'user_id' => '9436992',
	 *         'screen_name' => 'abraham' )
	 *
	 * @param string|mixed $oauth_verifier parameter returned by Twitter to verify connection.
	 * @return array containing oauth_token, oauth_token_secret, user_id, and screen_name for future API calls.
	 */
	function get_access_token( $oauth_verifier = false ) {
		$parameters = array();
		if ( ! empty( $oauth_verifier ) ) {
			$parameters['oauth_verifier'] = $oauth_verifier;
		}
		$request = $this->oAuthRequest( $this->accessTokenURL(), 'GET', $parameters );
		$token = OAuthUtil::parse_parameters( $request );
		$this->token = new OAuthConsumer( $token['oauth_token'], $token['oauth_token_secret'] );
		return $token;
	}

	/**
	 * One time exchange of username and password for access token and secret.
	 *
	 * @param string $username User's username.
	 * @param string $password User's password.
	 * @returns array("oauth_token" => "the-access-token",
	 *                "oauth_token_secret" => "the-access-secret",
	 *                "user_id" => "9436992",
	 *                "screen_name" => "abraham",
	 *                "x_auth_expires" => "0")
	 */
	function get_xauth_token( $username, $password ) {
		$parameters = array();
		$parameters['x_auth_username'] = $username;
		$parameters['x_auth_password'] = $password;
		$parameters['x_auth_mode'] = 'client_auth';
		$request = $this->oAuthRequest( $this->accessTokenURL(), 'POST', $parameters );
		$token = OAuthUtil::parse_parameters( $request );
		$this->token = new OAuthConsumer( $token['oauth_token'], $token['oauth_token_secret'] );
		return $token;
	}


	/**
	 * A GET wrapper for oAuthRequest
	 *
	 * @param string $url containing the intended API URL to request against.
	 * @param array  $parameters relevant to the API call.
	 * @return API|mixed The returned response from the Twitter API.
	 */
	function get( $url, $parameters = array() ) {
		$response = $this->oAuthRequest( $url, 'GET', $parameters );
		if ( 'json' === $this->format && $this->decode_json ) {
			return json_decode( $response );
		}
		return $response;
	}


	/**
	 * A POST wrapper for oAuthRequest
	 *
	 * @param string $url containing the intended API URL to request against.
	 * @param array  $parameters relevant to the API call.
	 * @return API|mixed The returned response from the Twitter API.
	 */
	function post( $url, $parameters = array() ) {
		$response = $this->oAuthRequest( $url, 'POST', $parameters );
		if ( 'json' === $this->decode_json && $this->format ) {
			return json_decode( $response );
		}
		return $response;
	}

	/**
	 * A DELETE wrapper for oAuthRequest
	 *
	 * @param string $url containing the intended API URL to request against.
	 * @param array  $parameters relevant to the API call.
	 * @return API|mixed The returned response from the Twitter API.
	 */
	function delete( $url, $parameters = array() ) {
		$response = $this->oAuthRequest( $url, 'DELETE', $parameters );
		if ( 'json' === $this->decode_json && $this->format ) {
			return json_decode( $response );
		}
		return $response;
	}

	/**
	 * Format and sign an OAuth / Twitter API request
	 *
	 * @param string $url containing the command to be issued to the API.
	 * @param string $method HTTP method to be used in the request.
	 * @param array  $parameters Additional parameters to be sent with the command to the API.
	 * @return API The returned response from the Twitter API.
	 */
	function oAuthRequest( $url, $method, $parameters ) {
		if ( strrpos( $url, 'https://' ) !== 0 && strrpos( $url, 'http://' ) !== 0 ) {
			$url = "{$this->host}{$url}.{$this->format}";
		}
		$request = OAuthRequest::from_consumer_and_token( $this->consumer, $this->token, $method, $url, $parameters );
		$request->sign_request( $this->sha1_method, $this->consumer, $this->token );
		switch ( $method ) {
			case 'GET':
			return $this->http( $request->to_url(), 'GET' );
			default:
		return $this->http( $request->get_normalized_http_url(), $method, $request->to_postdata() );
		}
	}

	/**
	 * Make the actual HTTP request
	 *
	 * @param string      $url containing the final URL to make a request to.
	 * @param string      $method HTTP method to be used in the request.
	 * @param string|null $postfields string of parameters to send with a POST request.
	 * @return mixed response from the Twitter API.
	 */
	function http( $url, $method, $postfields = null ) {
		$this->http_info = null;
		// this is never used.
		$options = array(
			'method' => $method,
			'timeout' => $this->timeout,
			'user-agent' => $this->useragent,
			'sslverify' => $this->ssl_verifypeer,
		);
		switch ( $method ) {
			case 'POST':
				if ( ! empty( $postfields ) ) {
					$options['body'] = $postfields;
				}
				break;
			case 'DELETE':
				if ( ! empty( $postfields ) ) {
					$url = "{$url}?{$postfields}";
				}
		}
		$response = wp_remote_request( $url, $options );
		if ( is_wp_error( $response ) ) {
			$this->http_code = null;
			$this->http_header = array();
			return false;
		}
		$this->http_code = $response['response']['code'];
		$this->http_header = $response['headers'];
		return $response['body'];
	}

	/**
	 * Callback function for cURL that stores the response headers.
	 *
	 * @param resource $ch cURL resource.
	 * @param string   $header Response headers from an HTTP request.
	 * @return int length of the returned header.
	 */
	function getHeader( $ch, $header ) {
		$i = strpos( $header, ':' );
		if ( ! empty( $i ) ) {
			$key = str_replace( '-', '_', strtolower( substr( $header, 0, $i ) ) );
			$value = trim( substr( $header, $i + 2 ) );
			$this->http_header[ $key ] = $value;
		}
		return strlen( $header );
	}
}
