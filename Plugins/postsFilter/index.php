<?php
/*
Plugin Name: Posts Filter by Category
Plugin URI: http://logicsbuffer.com
Description: Developed by LogicsBuffer
Version: 1.0
Author: Mustafa Jamal
Author URI: http://mustafajamal.logicsbuffer.com
*/

/* add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_filter_restrict_manage_posts' );
/**
 * First create the dropdown
 * make sure to change POST_TYPE to the name of your custom post type
 * 
 * @author Ohad Raz
 * 
 * @return void
 */
function check_user_role($roles, $user_id = null) {
	if ($user_id) $user = get_userdata($user_id);
	else $user = wp_get_current_user();
	if (empty($user)) return false;
	foreach ($user->roles as $role) {
		if (in_array($role, $roles)) {
			return true;
		}
	}
	return false;
}
add_action('admin_menu', 'wpdocs_register_my_custom_submenu_page');
 
function wpdocs_register_my_custom_submenu_page() {
	$user = wp_get_current_user();
	$user_role = $user->roles;
	$current_role = $user_role[0];
	
		$roles_news_updates = array('newseditor','administrator');
		if (in_array($current_role, $roles_news_updates)) {
			add_menu_page('News Updates','News Updates', 'edit_posts','edit.php?category_name=news-updates');
		}		
		
		$roles_news_updates = array('writer','administrator');
		if (in_array($current_role, $roles_news_updates)) {
			add_menu_page('Articles','Articles','edit_posts','edit.php?category_name=articles');
			add_menu_page('Blog','Blog','edit_posts','edit.php?category_name=blog');
			add_menu_page('Magazine','Magazine','edit_posts','edit.php?category_name=magazine');
			add_menu_page('Editorial','Editorial','edit_posts','edit.php?category_name=editorial');
		}
		
		$roles_reporter = array('reporter','administrator');
		if (in_array($current_role, $roles_reporter)) {
			add_menu_page('Reports','Reports','edit_posts','edit.php?category_name=reports');
		}
		
		$roles_featurewriter = array('featurewriter','administrator');
		if (in_array($current_role, $roles_featurewriter)) {		
			add_menu_page('Features','Features','edit_posts','edit.php?category_name=features');	
		}
		
		$roles_interviewer = array('interviewer','administrator');
		if (in_array($current_role, $roles_interviewer)) {
			add_menu_page('Interviews','Interviews','edit_posts','edit.php?category_name=interviews');
		}
		
		$roles_blogger = array('blogger','administrator');
		if (in_array($current_role, $roles_blogger)) {
			add_menu_page('Blog','Blog','edit_posts','edit.php?category_name=blog');
		}
		
		$roles_libraryeditor = array('libraryeditor','administrator');
		if (in_array($current_role, $roles_libraryeditor)) {
			add_menu_page('Library','Library','edit_posts','edit.php?category_name=library');
		}
		
		$roles_magazineeditor = array('magazineeditor','administrator');
		if (in_array($current_role, $roles_magazineeditor)) {
			add_menu_page('Magazine','Magazine','edit_posts','edit.php?category_name=magazine');
		}
		
		$roles_editorialeditor = array('editorialeditor','administrator');
		if (in_array($current_role, $roles_editorialeditor)) {
			add_menu_page('Editorial','Editorial','edit_posts','edit.php?category_name=editorial');
		}
		
		$roles_photoeditor = array('photoeditor','administrator');
		if (in_array($current_role, $roles_photoeditor)) {		
			add_menu_page('In Pictures','In Pictures','edit_posts','edit.php?category_name=in-pictures');
		}
		
		$roles_videoeditor = array('videoeditor','administrator');
		if (in_array($current_role, $roles_videoeditor)) {
			add_menu_page('In Videos','In Videos','edit_posts','edit.php?category_name=in-videos');
		}
		
		$roles_add_ins = array('add-inseditor','administrator');
		if (in_array($current_role, $roles_add_ins)) {
			add_menu_page('Add-ins','Add ins','edit_posts','edit.php?category_name=add-ins');
		}
			
}
function posts_for_current_author($query) {
        global $pagenow;
  
    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;
  
    if( !current_user_can( 'manage_options' ) ) {
       global $user_ID;
       $query->set('author', $user_ID );
     }
     return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');